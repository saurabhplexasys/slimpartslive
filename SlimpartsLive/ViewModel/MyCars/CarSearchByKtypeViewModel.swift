//
//  CarSearchByKtypeViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 22/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class CarSearchByKtypeViewModel {
    
    func getTypeCodeAPiCall(typeCode: String, completion: @escaping(Bool, String, String) -> Void) {
        
        AppInstance.showLoader()
        
        let url = Slimparts.baseURL + Slimparts.carSearchByKtypeURL + typeCode
        
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["ResponseCode"] as? String, let message = responseObj["ResponseText"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time", "")
                    return
                }
                if status == "1" {
                    let _data = responseObj["data"] as? [String: Any]
                    let _cars = _data!["cars"] as? [String: Any]
                    let _typecCode = _cars!["typecode"] as? String
                    
                    AppInstance.hideLoader()
                    completion(true, message, _typecCode ?? "")
                } else {
                    AppInstance.hideLoader()
                    completion(false, message, "")
                }  
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time", "")
                
            }
            
        }
        
    }
    
}
