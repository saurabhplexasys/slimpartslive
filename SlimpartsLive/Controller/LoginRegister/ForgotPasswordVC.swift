//
//  ForgotPasswordVC.swift
//  Slimparts
//
//  Created by Dharmesh Kothari on 21/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var emailTxt: ShakingTextField!
    @IBOutlet weak var newPassTxt: ShakingTextField!
    @IBOutlet weak var confPassTxt: ShakingTextField!
    
    let apiHit = ForgotPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        emailTxt.delegate = self
//        newPassTxt.delegate = self
//        confPassTxt.delegate = self
        
    }
    
    @IBAction func cancelBtnAction(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func setPassBtnAction(sender: UIButton) {
        self.fieldValidation()
    }
    
    func fieldValidation() {
        guard let emailT = self.emailTxt.text , self.emailTxt.text?.count != 0 else {
            self.showAlert(title: "Alert", message: "Please enter your email address")
            return
        }
        if isValidEmail(emailID: emailT) == false {
            self.showAlert(title: "Alert", message: "Please enter valid email address")
        } else {
            self.forPassApiCall()
        }
    }
    
    func forPassApiCall() {
        apiHit.forgotPasswordApiCall(email: emailTxt.text!, authKey: Slimparts.authKey) { (_isTrue, _message) in
            if _isTrue {
                self.showAlertAction(title: "Success", message: _message, alertStyle: .alert, actionTitles: ["Ok"], actionStyles: [.default], actions: [{_ in
                self.dismiss(animated: true, completion: nil)
                }])
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }

}

extension ForgotPasswordVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTxt {
            emailTxt.shake()
        }
//        if textField == newPassTxt {
//            newPassTxt.shake()
//        }
//        if textField == confPassTxt {
//            confPassTxt.shake()
//        }
    }
    
}
