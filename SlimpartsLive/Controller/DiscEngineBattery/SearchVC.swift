//
//  SearchVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 20/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class SearchVC: UIViewController, PassPicker {

    func passingObject(data: String, code: String, extra: String, staticPicker: String) {
        if staticPicker == "make" {
            // code for make
            print("data is \(data), code is \(code), string is \(staticPicker)")
            self.makeLble.text = data
            self.makeName = data
            self.makeCode = code
            
        } else if staticPicker == "model" {
            // code for model
            print("model name:- \(data), model code:- \(code)")
            self.modelLbl.text = data
            self.modelName = data
            self.modelCode = code
            
        } else if staticPicker == "engine" {
            // code for engine
            self.engineTypeLbl.text = data
            self.engineName = data
            self.engineCode = code
            self.extraKtype = extra
            print("engine name:- \(data), engine code:- \(code), engine ktype code:- \(extra)")
        }
    }
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    
    @IBOutlet weak var enterRegTxt: UITextField!
    @IBOutlet weak var makeLble: UILabel!
    @IBOutlet weak var modelLbl: UILabel!
    @IBOutlet weak var engineTypeLbl: UILabel!
    
    @IBOutlet weak var searchRegBtnOutlet: UIButton!
    @IBOutlet weak var searchVehicleBtnOutlet: UIButton!
    @IBOutlet weak var clearBtnOutlet: UIButton!
    
    
    let cartButton = SSBadgeButton()
    var delegatePassData: PassPicker!
    
    var makeUser = MakeViewModel()
    var modelUser = ModelViewModel()
    var engineUser = EngineTypeViewModel()
    var searchUser = SearchKTypeViewModel()
    var searchByReg = SearchByRegViewModel()
    
    var makeName = ""
    var makeCode = ""
    var modelName = ""
    var modelCode = ""
    var engineName = ""
    var engineCode = ""
    var extraKtype = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.setMenuBarBtn()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cartBtnView()
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func cartBtnView() {
        cartButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        cartButton.setImage(UIImage(named: "cartIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cartButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        cartButton.tintColor = .white
        
        cartButton.addTarget(self, action: #selector(cartButtonTapped), for: .touchUpInside)
        if let count = KeychainWrapper.standard.string(forKey: "notiCount") {
            cartButton.badgeBackgroundColor = .red
            cartButton.badge = count
        } else {
            cartButton.badgeBackgroundColor = .clear
            cartButton.badge = ""
        }
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: cartButton)]
    }
    
    
    
    @objc func cartButtonTapped() {
        print("cart button pressed")
        self.moveToCartScreen()
    }
    
    func moveToCartScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func searchRegistrationBtnAction(sender: UIButton) {
        print("search registration button pressed")
        if enterRegTxt.text!.count > 1 {
            self.searchByRegisAPiCall()
        } else {
            self.showAlert(title: "Alert", message: "enter correct registration number..")
        }
    }
    
    func searchByRegisAPiCall() {
        searchByReg.searchByReg(regCode: enterRegTxt.text!) { (_isTrue, _message, _searchData) in
            if _isTrue {
                print("search by reg data:- ", _searchData)
                self.showAlert(title: "Wait!", message: "Search by Registration No - In Progress...")
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    
    
    @IBAction func makeBtnAction(sender: UIButton) {
        print("make button pressed")
        DispatchQueue.main.async {
            self.makeAPICall()
        }
    }
    
    func makeAPICall() {
        makeUser.getMakeData { (_isTrue, _message, _makeData) in
            if _isTrue {
                let makeList = _makeData.data
                var data = [String]()
                data.removeAll()
                var code = [String]()
                code.removeAll()
                
                for list in makeList! {
                    data.append(list.makename ?? "No Data")
                    code.append(list.makecode ?? "No Data")
                }
                let str = "make"
                self.moveToPickerVC(data: data, code: code, extra: [""], staticString: str)
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    func moveToPickerVC(data: [String], code: [String], extra: [String], staticString: String) {
        if #available(iOS 13.0, *) {
            let pickerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PickerVC") as! PickerVC
            pickerVC.delegatePassData = self
            pickerVC.pickerArrayData = data
            pickerVC.pickerArrayCode = code
            pickerVC.pickerArrayExtra = extra
            pickerVC.staticPicker = staticString
            if staticString == "engine" {
                pickerVC.extraBool = true
            } else {
                pickerVC.extraBool = false
            }
            self.present(pickerVC, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let pickerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PickerVC") as! PickerVC
            pickerVC.delegatePassData = self
            pickerVC.pickerArrayData = data
            pickerVC.pickerArrayCode = code
            pickerVC.pickerArrayExtra = extra
            pickerVC.staticPicker = staticString
            if staticString == "engine" {
                pickerVC.extraBool = true
            } else {
                pickerVC.extraBool = false
            }
            self.present(pickerVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func modelBtnAction(sender: UIButton) {
        print("model button pressed")
        if makeLble.text != "Make" {
            DispatchQueue.main.async {
                self.modelAPICall()
            }
        } else {
            self.showAlert(title: "Please Select Make", message: "")
        }
    }
    
    func modelAPICall() {
        modelUser.getModelData(makeCode: makeCode) { (_isTrue, _message, _modelData) in
            if _isTrue {
                let modelList = _modelData
                var data = [String]()
                data.removeAll()
                var code = [String]()
                code.removeAll()
                
                for list in modelList {
                    data.append(list.modelName ?? "No Name")
                    code.append(list.modelCode ?? "No Code")
                }
                let str = "model"
                self.moveToPickerVC(data: data, code: code, extra: [""], staticString: str)
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    
    
    @IBAction func engineTypeBtnAction(sender: UIButton) {
        print("engine type button pressed")
        if modelLbl.text != "Model" {
            DispatchQueue.main.async {
                self.engineTypeApiCall()
            }
        } else {
            self.showAlert(title: "Please Select Model", message: "")
        }
    }
    
    func engineTypeApiCall() {
        engineUser.getEngineTypeData(modelCode: modelCode) { (_isTrue, _message, _engineData) in
            if _isTrue {
                let engineList = _engineData
                var data = [String]()
                data.removeAll()
                var code = [String]()
                code.removeAll()
                var ktype = [String]()
                ktype.removeAll()
                
                for list in engineList {
                    data.append(list.typeName ?? "No Name")
                    code.append(list.typeCode ?? "No Code")
                    ktype.append(list.kTypeCode ?? "No data")
                }
                let str = "engine"
                self.moveToPickerVC(data: data, code: code, extra: ktype, staticString: str)
                
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    @IBAction func searchVehicleBtnAction(sender: UIButton) {
        print("search vehicle button pressed")
        if makeLble.text != "Make" && modelLbl.text != "Model" && engineTypeLbl.text != "Select an engine type" {
            self.searchAPICall()
        } else {
            self.showAlert(title: "Alert", message: "Select Make, Model and Engine type")
        }
    }
    
    func searchAPICall() {
        searchUser.searchByKtype(kTypecode: extraKtype) { (_isTrue, _message, _searchData) in
            if _isTrue {
                
                print("Search Data is:- ", _searchData)
                self.showAlert(title: "Wait!", message: "Search bu Vehicle - In Progress...")
                
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    @IBAction func clearBtnAction(sender: UIButton) {
        print("clear button pressed")
        self.makeLble.text = "Make"
        self.modelLbl.text = "Model"
        self.engineTypeLbl.text = "Select an engine type"
    }
    
    
    
}
