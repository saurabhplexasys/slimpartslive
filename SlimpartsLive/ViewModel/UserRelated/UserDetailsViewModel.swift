//
//  UserDetailsViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 01/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class UserDetailsViewModel {
    
    func getUserDatailsAPI(userId: String, completion: @escaping(Bool, String, UserDetailsModel) -> Void) {
        
        AppInstance.showLoader()
        
        let url = Slimparts.baseURL + Slimparts.getUserDetailsURL + userId
        
        var addressDetails = [AddressDetails]()
        addressDetails.removeAll()
        var userDetails = UserDetailsModel(firstName: "", lastName: "", email: "", mobile: "", addressDetail: addressDetails)
        
        var firstName = ""
        var lastName = ""
        var email = ""
        var mobile = ""
        
        AF.request(url).responseJSON { (response) in
            switch response.result {
            case .success:
                let responseObj = response.value as? [String: Any]
                let _data = responseObj!["data"] as? [String: Any]
                let _userD = _data!["userDetail"] as? [[String: Any]]
                let _addressD = _data!["addressDetail"] as? [[String: Any]]
                
                if !_userD!.isEmpty {
                    for userD in _userD! {
                        let _firstName = userD["first_name"] as? String
                        let _lastName = userD["last_name"] as? String
                        let _email = userD["email"] as? String
                        let _mobile = userD["mobile"] as? String
                        firstName = _firstName ?? ""
                        lastName = _lastName ?? ""
                        email = _email ?? ""
                        mobile = _mobile ?? ""
                    }
                }
                
                if !_addressD!.isEmpty {
                    for addrD in _addressD! {
                        let _address = addrD["address"] as? String
                        let _city = addrD["city"] as? String
                        let _postcode = addrD["postcode"] as? String
                        let _addType = addrD["address_type"] as? String
                        let _houseNo = addrD["house_number"] as? String
                        let _addId = addrD["id"] as? Int
                        
                        let addData = AddressDetails(address: _address ?? "", city: _city ?? "", postCode: _postcode ?? "", addressId: _addId ?? 0, addressType: _addType ?? "", houseNo: _houseNo ?? "")
                        addressDetails.append(addData)
                    }
                }
                
                userDetails = UserDetailsModel(firstName: firstName, lastName: lastName, email: email, mobile: mobile, addressDetail: addressDetails)
                AppInstance.hideLoader()
                completion(true, "", userDetails)
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "serverProblem", userDetails)
            }
        }
        
        
    }
    
}
