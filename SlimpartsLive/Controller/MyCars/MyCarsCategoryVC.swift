//
//  MyCarsCategoryVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 21/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class MyCarsCategoryVC: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var carNameLbl: UILabel!
    
    let cartButton = SSBadgeButton()
    
    var userView = MyCarCategoryViewModel()
    var catData: MyCarCategoryModel?
    var searchData = [MenuItemCatModel]()
    
    var carList: CarListModel?
    var makeName = ""
    var modelName = ""
    var engineType = ""
    var kTypeCode = ""
    var carNameTitle = ""
    
    var vaiCar = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.setMenuBarBtn()
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationController?.navigationBar.tintColor = .white
        self.setCarName()
        if kTypeCode == "" {
            if let typeCode = KeychainWrapper.standard.string(forKey: "typeCode") {
                self.kTypeCode = typeCode
            }
        }
        self.carCategorySubCatApiCall()

        // Do any additional setup after loading the view.
    }
    
    func setCarName() {
        if makeName == "" {
            
            backBtnOutlet.isEnabled = false
            backBtnOutlet.image = nil
            
            if let makeN = KeychainWrapper.standard.string(forKey: "makeName") {
                makeName = makeN
            }
            if let modelN = KeychainWrapper.standard.string(forKey: "modelName") {
                modelName = modelN
            }
            if let engineN = KeychainWrapper.standard.string(forKey: "engineName") {
                engineType = engineN
            }
            carNameTitle = "\(makeName) > \(modelName) > \(engineType)"
            if carNameTitle == " >  > " {
                self.carNameLbl.text = ""
            } else {
                self.carNameLbl.text = carNameTitle
            }
        } else {
            let makeN = makeName
            let modelN = modelName
            let engineT = engineType
            carNameTitle = "\(makeN) > \(modelN) > \(engineT)"
            if carNameTitle == " >  > " {
                self.carNameLbl.text = ""
            } else {
                self.carNameLbl.text = carNameTitle
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cartBtnView()
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func cartBtnView() {
        cartButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        cartButton.setImage(UIImage(named: "cartIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cartButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        cartButton.tintColor = .white
        
        cartButton.addTarget(self, action: #selector(cartButtonTapped), for: .touchUpInside)
        if let count = KeychainWrapper.standard.string(forKey: "notiCount") {
            cartButton.badgeBackgroundColor = .red
            cartButton.badge = count
        } else {
            cartButton.badgeBackgroundColor = .clear
            cartButton.badge = ""
        }
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: cartButton)]
    }
    
    @objc func cartButtonTapped() {
        print("cart button pressed")
        self.moveToCartScreen()
    }
    
    func moveToCartScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func carCategorySubCatApiCall() {
        
        userView.getCarCatSubCatAPiCall { (_isTrue, _message, _data) in
            if _isTrue {
                self.catData = _data
                let cat = self.catData?.menuItem
                self.searchData = cat!
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            searchData = (catData?.menuItem)!
            self.tableView.reloadData()
            return
        }
        searchData = (catData?.menuItem!.filter({ (CatTitle) -> Bool in
            (CatTitle.menu?.lowercased().contains(searchText.lowercased()))!
        }))!
        self.tableView.reloadData()
    }
    
    
    
}

extension MyCarsCategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCarCategoryTVCell", for: indexPath) as! MyCarCategoryTVCell
        
        cell.titleLbl.text = self.searchData[indexPath.row].menu
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyCarsSubCategoryVC") as! MyCarsSubCategoryVC
            vc.catData = self.searchData[indexPath.row]
            vc.carList = self.carList
            vc.kTypeCode = self.kTypeCode
            vc.carNameTitle = self.carNameTitle
            vc.vaiCar = self.vaiCar
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyCarsSubCategoryVC") as! MyCarsSubCategoryVC
            vc.catData = self.searchData[indexPath.row]
            vc.carList = self.carList
            vc.kTypeCode = self.kTypeCode
            vc.carNameTitle = self.carNameTitle
            vc.vaiCar = self.vaiCar
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

