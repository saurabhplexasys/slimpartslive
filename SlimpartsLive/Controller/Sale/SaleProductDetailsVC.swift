//
//  SaleProductDetailsVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 06/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftKeychainWrapper

class SaleProductDetailsVC: UIViewController {
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var proName: UILabel!
    @IBOutlet weak var proCode: UILabel!
    @IBOutlet weak var proDesc: UILabel!
    @IBOutlet weak var proPrice: UILabel!
    @IBOutlet weak var outStockBtnOutlet: UIButton!
    @IBOutlet weak var quantityTF: UITextField!
    @IBOutlet weak var quantityView: BorderView!
    
    var proDetails: SaleProductModel?
    var userId = ""
    var cartItem: [[String: AnyObject]] = []
    var isItemAdded = false
    var userAddCart = AddToCartSaleProductViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.setMenuBarBtn()
        self.setProductDetailView()
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            userId = userid
        }
        
        // Do any additional setup after loading the view.
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func setProductDetailView() {
        
        self.proImg.sd_setImage(with: URL(string: proDetails?.proImg ?? "defaultImage")) { (image, error, cache, url) in
            if error != nil {
                self.proImg.image = UIImage(named: "defaultImage")
            } else {
                self.proImg.image = image
            }
        }
        self.proName.text = proDetails?.proName ?? ""
        self.proCode.text = proDetails?.proUniqId ?? ""
        self.proDesc.text = proDetails?.proDesc ?? ""
        self.proPrice.text = proDetails?.proPrice ?? ""
        
        if let quantity = proDetails?.proQuantity {
            if quantity > 0 {
                self.outStockBtnOutlet.setTitle("Add to Cart", for: .normal)
                self.outStockBtnOutlet.setTitleColor(UIColor.init(displayP3Red: 00/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                self.quantityView.isHidden = false
            } else {
                self.outStockBtnOutlet.setTitle("Out Of Stock", for: .normal)
                self.outStockBtnOutlet.setTitleColor(.orange, for: .normal)
                self.quantityView.isHidden = true
            }
        } else {
            self.outStockBtnOutlet.setTitle("Out Of Stock", for: .normal)
            self.quantityView.isHidden = true
        }
        
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func minusBtnAction(_ sender: UIButton) {
        print("minus btn press")
        let quantity = proDetails?.proQuantity
        let quaTxt = Int(quantityTF.text ?? "1")
        if quaTxt == 1 {
            quantityTF.text = "1"
        } else if quaTxt! >= 1 || quaTxt! <= quantity ?? 1{
            quantityTF.text = String(quaTxt! - 1)
        } else {
            quantityTF.text = "1"
        }
    }
    
    @IBAction func plusBtnAction(_ sender: UIButton) {
        print("plus btn press")
        let quantity = proDetails?.proQuantity
        let quaTxt = Int(quantityTF.text ?? "1")
        if quaTxt! <= quantity ?? 1{
            if quaTxt == quantity {
                self.showAlert(title: "You can not add more than \(quaTxt ?? 0) units", message: "")
            } else {
                let out = (quaTxt! + 1)
                quantityTF.text = String(out)
            }
        } else {
            quantityTF.text = "1"
        }
    }
    
    
    @IBAction func addToCart(_ sender: UIButton) {
        
        if self.outStockBtnOutlet.titleLabel?.text == "Add to Cart" {
            // add to cart functionality
            print("****** Product add to cart *******")
            if userId == "" {
                //if not get userId then add product to locally
                
                var productImage = ""
                if let proImage = proDetails?.proImg {
                    productImage = proImage
                }
                let quantity = self.quantityTF.text
                
                if let loadedCart = UserDefaults().array(forKey: "myCart") as? [[String: AnyObject]] {
                    for item in loadedCart {
                        if proDetails?.proId == item["proId"] as? Int {
                            isItemAdded = true
                        }
                    }
                }
                if isItemAdded == true {
                    isItemAdded = false
                    self.showAlert(title: "item already added", message: "")
                } else {
                    isItemAdded = false
                    cartItem.append(["proId": proDetails?.proId as AnyObject, "proUniqId": proDetails?.proUniqId as AnyObject, "proName": proDetails?.proName as AnyObject, "proPrice": proDetails?.proPrice as AnyObject, "proDesc": proDetails?.proDesc as AnyObject, "proImg": productImage as AnyObject, "proQuantity": quantity as AnyObject, "status": proDetails?.status as AnyObject, "catId": proDetails?.catId as AnyObject, "subCatId": proDetails?.subCatId as AnyObject, "brand": proDetails?.brand as AnyObject])
                    
                    UserDefaults.standard.set(cartItem, forKey: "cartItem")
                    
                    if let saveItem = UserDefaults.standard.array(forKey: "cartItem") {
                        print("Save item are :- ", saveItem)
                    }
                    
                    let count: String = String(cartItem.count)
                    
                    KeychainWrapper.standard.set(count, forKey: "notiCount")
                    
                    self.moveBackAfterSaveToCart(_message: "item save successfully")
                    
                    
                    //To fetch above cartItem from user default
                    //                if let loadedCart = NSUserDefaults().arrayForKey("myCart") as? [[String: AnyObject]] {
                    //                    print(loadedCart)  // [[price: 19.99, qty: 1, name: A], [price: 4.99, qty: 2, name: B]]"
                    //
                    //                    for item in loadedCart {
                    //                        print(item["name"] as! String)  // A, B
                    //                        print(item["price"] as! Double) // 19.99, 4.99
                    //                        print(item["qty"] as! Int)      // 1, 2
                    //                    }
                    //                }
                }
                
            } else {
                //hit add to cart api: only login user can add to cart directly to server
                print("*****Hit add to cart api*****")
                self.addToCartAPICAll()
                
            }
        } else {
            print("****** Product out of stock *******")
        }
        
    }
     
    func moveBackAfterSaveToCart(_message: String) {
        self.showAlertAction(title: "Success", message: _message, alertStyle: .alert, actionTitles: ["Ok"], actionStyles: [.default], actions: [{_ in
            self.navigationController?.popViewController(animated: true)
            }])
    }
    
    func addToCartAPICAll() {
        
        let price = proDetails?.proPrice ?? "0.0"
        let priceDouble = Double(price)!
        let quan = self.quantityTF.text ?? "1"
        let quanInt = Int(quan)!
        
        let proId = proDetails?.proId ?? 0
        let proIdStr = String(proId)
        
        userAddCart.addToCartSaleProductApiCall(name: proDetails?.proName ?? "", price: priceDouble, marginPrice: 0, quantity: quanInt, vehicleName: proDetails?.proName ?? "", image: proDetails?.proImg ?? "", type: 0, uniCode: proDetails?.proUniqId ?? "", totalQuantity: proDetails?.proQuantity ?? 0, supCode: "0", userId: Int(self.userId)!, productId: proIdStr, itemNo: "", itemGroup: 0, btw: "", extraFee: "") { (_isTrue, _message) in
            if _isTrue {
                self.moveBackAfterSaveToCart(_message: _message)
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
        
    }
    
    
}
