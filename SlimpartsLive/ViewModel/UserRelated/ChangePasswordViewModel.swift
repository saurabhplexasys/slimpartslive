//
//  ChangePasswordViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 01/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class ChangePasswordViewModel {
    
    func changePasswordAPiCall(oldPass: String, newPass: String, confPass: String, userId: String, completion: @escaping(Bool, String)-> Void) {
        
        AppInstance.showLoader()
        let url = Slimparts.baseURL + Slimparts.changePasswordUrl
        let parameter = ["old_password": oldPass, "new_password": newPass, "confirm_password": confPass, "user_id": userId]
        
        AF.request(url, method: .post, parameters: parameter).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any] else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! pleae try after some time")
                    return
                }
                AppInstance.hideLoader()
                completion(true, "Password changed successfully")
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time")
            }
        }
        
    }
    
}
