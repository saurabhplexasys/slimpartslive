//
//  MyOrderViewTVCellTableViewCell.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 02/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class MyOrderViewTVCell: UITableViewCell {

    @IBOutlet weak var proNameQuantityLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
