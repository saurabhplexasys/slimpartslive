//
//  MyCarsProductVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 21/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftKeychainWrapper

class MyCarsProductVC: UIViewController {
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iIconBtn: UIButton!
    
    let cartButton = SSBadgeButton()
    
    var userId = ""
    var kTypeCode = ""
    var menuCode = ""
    var partCode = ""
    var carNameTitle = ""
    
    var catId = ""
    var subCatId = ""
    var cartItem: [[String: AnyObject]] = []
    var isItemAdded = false
    
    var user = CarPartsViewModel()
    var carProData = [CarPartsModel]()
    var userAddCart = AddToCartSaleProductViewModel()
    
    var vaiCar = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 162
        self.navigationController?.navigationBar.tintColor = .white
        self.setMenuBarBtn()
        self.cartBtnView()
        
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            userId = userid
        }
        
        if vaiCar == true {
            // then menucode get in category api
            print("selected car menu code is:- ", menuCode)
        } else {
            // menucode get from side menu selected option
            if let code = KeychainWrapper.standard.string(forKey: "menuCode") {
                menuCode = code
                print("selected menu code is:- ", menuCode)
            }
        }
        
        self.titleLbl.text = carNameTitle
        self.getCarProductAPiCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateCartCount()
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func cartBtnView() {
        cartButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        cartButton.setImage(UIImage(named: "cartIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cartButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        cartButton.tintColor = .white
        
        cartButton.addTarget(self, action: #selector(cartButtonTapped), for: .touchUpInside)
        
        self.updateCartCount()
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: cartButton)]
    }
    
    func updateCartCount() {
        if let count = KeychainWrapper.standard.string(forKey: "notiCount") {
            cartButton.badgeBackgroundColor = .red
            cartButton.badge = count
        } else {
            cartButton.badgeBackgroundColor = .clear
            cartButton.badge = ""
        }
    }
    
    @objc func cartButtonTapped() {
        print("cart button pressed")
        self.moveToCartScreen()
    }
    
    func moveToCartScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getCarProductAPiCall() {
        user.getCarProductAPiCall(typeCode: kTypeCode, menuCode: menuCode, partCode: partCode) { (_isTrue, _message, _data) in
            if _isTrue {
                print("Success API")
                self.carProData = _data
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func iIconBtnAction(_ sender: UIButton) {
        
        
    }
    
    
}

extension MyCarsProductVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carProData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCarProductTVCell", for: indexPath) as! MyCarProductTVCell
        
        if let proName = carProData[indexPath.row].sup {
            cell.proNameLbl.text = proName
        } else {
            cell.proNameLbl.text = "No Name"
        }
        if let price = carProData[indexPath.row].price {
            if price == "0.00" {
                cell.proPriceLbl.text = ""
            } else {
                cell.proPriceLbl.text = "Price: \(price)"
            }
        }
        if let addCart = carProData[indexPath.row].addToCart {
            if addCart == "1" {
                //cell.addToCartView.isHidden = false
                cell.addToCartBtn.setTitle("Add to Cart", for: .normal)
                cell.addToCartBtn.setTitleColor(UIColor.init(displayP3Red: 0/255, green: 84/255, blue: 147/255, alpha: 1), for: .normal)
            } else {
                //cell.addToCartView.isHidden = true
                cell.addToCartBtn.setTitle("Notify Me!", for: .normal)
                cell.addToCartBtn.setTitleColor(UIColor.init(displayP3Red: 0/255, green: 116/255, blue: 0/255, alpha: 1), for: .normal)
            }
        } else {
            cell.proPriceLbl.text = ""
            cell.addToCartView.isHidden = true
        }
        if let sku = carProData[indexPath.row].art {
            cell.proSkuLbl.text = "SKU: \(sku)"
        } else {
            cell.proSkuLbl.text = "SKU: -"
        }
        if let refrem = carProData[indexPath.row].refrem {
            let refremPlus = "\u{2022}  " + refrem
            
            let refremFinal = refremPlus.replacingOccurrences(of: ";", with: "\n\u{2022} ")
            cell.proRefremLbl.text = refremFinal
        }
        cell.quantityView.isHidden = true
        
        cell.addToCartBtn.tag = indexPath.row
        cell.addToCartBtn.addTarget(self, action: #selector(addToCartAction(_:)), for: .touchUpInside)
        cell.viewDetailBtnOutlet.tag = indexPath.row
        cell.viewDetailBtnOutlet.addTarget(self, action: #selector(viewDetialsAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func viewDetialsAction(_ sender: UIButton) {
        print("View Details Btn Pressed")
                if #available(iOS 13.0, *) {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyCarsProductDetailsVC") as! MyCarsProductDetailsVC
                    vc.proDetails = carProData[sender.tag]
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyCarsProductDetailsVC") as! MyCarsProductDetailsVC
                    vc.proDetails = carProData[sender.tag]
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
        
    }
    
    @objc func addToCartAction(_ sender: UIButton) {
        
        let index = sender.tag
        
        let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MyCarProductTVCell
        
        print("Add to cart btn clicked")
        if cell.addToCartBtn.titleLabel?.text == "Add to Cart" {
            // add to cart functionality
            print("****** Product add to cart *******")
            if userId == "" {
                //if not get userId then add product to locally
                
                var productImage = ""
                if let proImage = carProData[index].pic {
                    productImage = proImage
                }
                let quantity = "1"
                
                if let loadedCart = UserDefaults().array(forKey: "myCart") as? [[String: AnyObject]] {
                    for item in loadedCart {
                        let proId = "0"
                        if Int(proId) == item["proId"] as? Int {
                            isItemAdded = true
                        }
                    }
                }
                if isItemAdded == true {
                    isItemAdded = false
                    self.showAlert(title: "item already added", message: "")
                } else {
                    isItemAdded = false
                    cartItem.append(["proId": "1" as AnyObject, "proUniqId": carProData[index].supCode as AnyObject, "proName": carProData[index].sup as AnyObject, "proPrice": carProData[index].price as AnyObject, "proDesc": carProData[index].refrem as AnyObject, "proImg": productImage as AnyObject, "proQuantity": quantity as AnyObject, "status": "" as AnyObject, "catId": self.catId as AnyObject, "subCatId": self.subCatId as AnyObject, "brand": carProData[index].sup as AnyObject])
                    
                    UserDefaults.standard.set(cartItem, forKey: "cartItem")
                    
                    if let saveItem = UserDefaults.standard.array(forKey: "cartItem") {
                        print("Save item are :- ", saveItem)
                    }
                    
                    let count: String = String(cartItem.count)
                    
                    KeychainWrapper.standard.set(count, forKey: "notiCount")
                    
                    self.showAlert(title: "Success", message: "item save successfully")
                    
                    //To fetch above cartItem from user default
                    //                if let loadedCart = NSUserDefaults().arrayForKey("myCart") as? [[String: AnyObject]] {
                    //                    print(loadedCart)  // [[price: 19.99, qty: 1, name: A], [price: 4.99, qty: 2, name: B]]"
                    //
                    //                    for item in loadedCart {
                    //                        print(item["name"] as! String)  // A, B
                    //                        print(item["price"] as! Double) // 19.99, 4.99
                    //                        print(item["qty"] as! Int)      // 1, 2
                    //                    }
                    //                }
                }
                
            } else {
                //hit add to cart api: only login user can add to cart directly to server
                print("*****Hit add to cart api*****")
                self.addToCartAPICAll(index: sender.tag)
                
            }
        } else {
            print("****** Product out of stock : Please notify me when available *******")
        }
        
    }
    
    func addToCartAPICAll(index: Int) {
        let price = carProData[index].price ?? "0.0"
        let priceDouble = Double(price)!
        let quan = "1"
        let quanInt = Int(quan)!
        
        let itemG = carProData[index].itemGroup ?? "0"
        let itemGInt: Int = Int(itemG)!
        
        userAddCart.addToCartSaleProductApiCall(name: carProData[index].sup ?? "", price: priceDouble, marginPrice: 0, quantity: quanInt, vehicleName: carProData[index].sup ?? "", image: carProData[index].pic ?? "", type: 0, uniCode: carProData[index].art ?? "", totalQuantity: carProData[index].stock ?? 0, supCode: "0", userId: Int(self.userId)!, productId: "0", itemNo: carProData[index].itemNo ?? "", itemGroup: itemGInt, btw: carProData[index].btw ?? "", extraFee: carProData[index].extraFee ?? "") { (_isTrue, _message) in
            if _isTrue {
                self.updateCartCount()
                self.showAlert(title: "Success", message: _message)
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
        
        
    }
    
    
    
    
    
}

