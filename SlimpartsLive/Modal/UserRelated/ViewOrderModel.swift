//
//  ViewOrderModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 01/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct ViewOrderModel {
    
    let user: UserViewOrderModel?
    let orders: OrderViewOrderModel?
    let cartList: [CartListViewOrderModel]?
    let address: [AddressViewOrderModel]?
    
}

struct UserViewOrderModel {
    let id: Int?
    let firstName: String?
    let lastName: String?
    let email: String?
    let mobile: String?
    let profileImg: String?
}

struct OrderViewOrderModel {
    
    let id: Int?
    let date: String?
    let orderNo: String?
    let subTotal: String?
    let shipping: String?
    let tax: String?
    let totalPrice: String?
    let status: String?
    
}

struct CartListViewOrderModel {
    let id: Int?
    let productId: Int?
    let productNAme: String?
    let price: String?
    let quantity: Int?
    let proImg: String?
    let proCode: String?
    let supCode: String?
}

struct AddressViewOrderModel {
    
    let id: Int?
    let address: String?
    let city: String?
    let postCode: String?
    let houseNo: String?
    let addType: String?
    
}

