//
//  AddToCartSaleProductViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 12/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

class AddToCartSaleProductViewModel {
    
    func addToCartSaleProductApiCall(name: String, price: Double, marginPrice: Double, quantity: Int, vehicleName: String, image: String, type: Int, uniCode: String, totalQuantity: Int, supCode: String, userId: Int, productId: String, itemNo: String, itemGroup: Int, btw: String, extraFee: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        
        let url = Slimparts.baseURL + Slimparts.addToCartSaleProductURL
        
        let parameter = ["name": name, "price": price, "margin_price": marginPrice, "count": quantity, "vehicleName": name, "image": image, "type": type, "code": uniCode, "total_quantity": totalQuantity, "supcode": supCode, "user_id": userId, "product_id": productId, "itemnumber": itemNo, "itemgroup": itemGroup, "btw": btw, "extrafees": extraFee] as [String : Any]
        
        
        AF.request(url, method: .post, parameters: parameter).responseJSON { (response) in
            
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any] else {
                    AppInstance.hideLoader()
                    completion(false, "Server Side Issue! try after some time")
                    return
                }
                if !responseObj.isEmpty {
                    
                    let status = responseObj["status"] as? String
                    
                    if status == "success" {
                        let msg = responseObj["msg"] as? String
                        let count = responseObj["count"] as? Int
                        let notiCount = String(count ?? 0)
                        KeychainWrapper.standard.set(notiCount, forKey: "notiCount")
                        AppInstance.hideLoader()
                        completion(true, msg ?? "")
                    } else {
                        AppInstance.hideLoader()
                        completion(false, "Something went wrong! please try after some time")
                    }
                } else {
                    AppInstance.hideLoader()
                    completion(false, "Something went wrong! please try after some time")
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! try after some time")
                
            }
            
        }
        
        
        
    }
    
}
