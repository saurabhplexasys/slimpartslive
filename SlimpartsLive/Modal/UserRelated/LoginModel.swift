//
//  LoginModel.swift
//  Slimparts
//
//  Created by Dharmesh Kothari on 22/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct LoginModel : Codable {
    let message : String?
    let status : String?
    let data : Data?

    enum CodingKeys: String, CodingKey {

        case message = "message"
        case status = "status"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        data = try values.decodeIfPresent(Data.self, forKey: .data)
    }

}

struct Data : Codable {
    let user_id : String?
    let first_name : String?
    let last_name : String?
    let email : String?
    let mobile : String?
    let creationdate : String?
    let updationdate : String?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case first_name = "first_name"
        case last_name = "last_name"
        case email = "email"
        case mobile = "mobile"
        case creationdate = "creationdate"
        case updationdate = "updationdate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        creationdate = try values.decodeIfPresent(String.self, forKey: .creationdate)
        updationdate = try values.decodeIfPresent(String.self, forKey: .updationdate)
    }

}
