//
//  ReturnProductListModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 05/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct RefundProductListModel {
    
    var id: Int?
    var proId: Int?
    var proName: String?
    var totalPrice: String?
    var quantity: Int?
    var proImg: String?
    var orderFor: String?
    var proCode: String?
    var supCode: String?
    var orderId: Int?
    var userId: Int?
    var refund: Int?
    var refundId: Int?
    
}
