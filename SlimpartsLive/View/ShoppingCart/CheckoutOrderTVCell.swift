//
//  CheckoutOrderTVCell.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 12/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class CheckoutOrderTVCell: UITableViewCell {

    @IBOutlet weak var proNameQuantityLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
