//
//  OrderHistoryViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 28/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class OrderHistoryViewModel {
    
    
    func orderHistoryApiCall(userId: String, completion: @escaping(Bool, String, [OrderHistoryModel]) -> Void) {
        
        AppInstance.showLoader()
        var orderData = [OrderHistoryModel]()
        orderData.removeAll()
        
        let url = Slimparts.baseURL + Slimparts.orderHistoryURL + userId
                
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [[String: Any]] else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time", orderData)
                    return
                }
                if !responseObj.isEmpty {
                    for data in responseObj {
                        let _date = data["created_at"] as? String
                        let _id = data["id"] as? Int
                        let _orderNo = data["order_num"] as? String
                        let _totalPrice = data["total"] as? String
                        let _status = data["status"] as? String
                        let _payStatus = data["payment_status"] as? String
                        let _deliveryDate = data["delivery_date"] as? String
                        let _invoiceNo = data["invoice_number"] as? Int
                        let _pdf = data["invoice_pdf"] as? String
                        
                        let product = OrderHistoryModel(date: _date ?? "", id: _id ?? 0, orderNo: _orderNo ?? "", total: _totalPrice ?? "", status: _status ?? "", payStatus: _payStatus ?? "", deliveryDate: _deliveryDate ?? "", invoiceNo: _invoiceNo ?? 0, invoicePdf: _pdf ?? "")
                        
                        orderData.append(product)
                    }
                    
                    AppInstance.hideLoader()
                    completion(true, "Success", orderData)
                } else {
                    AppInstance.hideLoader()
                    completion(false, "Data Not Found", orderData)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time", orderData)
            }
        }
        
        
    }
    
}
