//
//  SaleSubVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 04/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class SaleSubVC: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var catTitleLbl: UILabel!
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    
    let cartButton = SSBadgeButton()
    
    var catData: SaleModel?
    
    var subData = [SubCategoryAccessories]()
    var subSearchData = [SubCategoryAccessories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        self.setMenuBarBtn()
        
        self.catTitleLbl.text = catData?.catName ?? ""
        
        self.subData = (self.catData?.subCat)!
        self.subSearchData = self.subData
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cartBtnView()
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func cartBtnView() {
        cartButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        cartButton.setImage(UIImage(named: "cartIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cartButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        cartButton.tintColor = .white
        
        cartButton.addTarget(self, action: #selector(cartButtonTapped), for: .touchUpInside)
        if let count = KeychainWrapper.standard.string(forKey: "notiCount") {
            cartButton.badgeBackgroundColor = .red
            cartButton.badge = count
        } else {
            cartButton.badgeBackgroundColor = .clear
            cartButton.badge = ""
        }
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: cartButton)]
    }
    
    @objc func cartButtonTapped() {
        print("cart button pressed")
        self.moveToCartScreen()
    }
    
    func moveToCartScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            subSearchData = subData
            self.tableView.reloadData()
            return
        }
        
        subSearchData = subData.filter({ (SubTitle) -> Bool in (SubTitle.subCatName?.lowercased().contains(searchText.lowercased()))!
        })
        
        self.tableView.reloadData()
    }

}

extension SaleSubVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subSearchData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SaleSubTVCell", for: indexPath) as! SaleSubTVCell
        
        cell.titleLbl.text = subSearchData[indexPath.row].subCatName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("SubCategory Selected", subSearchData[indexPath.row])
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SaleProductVC") as! SaleProductVC
            vc.catData = self.catData
            vc.subCatData = subSearchData[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SaleProductVC") as! SaleProductVC
            vc.catData = self.catData
            vc.subCatData = subSearchData[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
