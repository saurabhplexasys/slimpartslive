//
//  MyOrdersVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 24/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class MyOrdersVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    
    var userOrder = OrderHistoryViewModel()
    
    var orderData = [OrderHistoryModel]()
    
    var userId = ""
    
    let cartButton = SSBadgeButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        tableView.delegate = self
        tableView.dataSource = self
        
        self.setMenuBarBtn()
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
            print("user id:- ", self.userId)
        }
        self.orderHistoryAPiCall()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cartBtnView()
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func cartBtnView() {
        cartButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        cartButton.setImage(UIImage(named: "cartIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cartButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        cartButton.tintColor = .white
        
        cartButton.addTarget(self, action: #selector(cartButtonTapped), for: .touchUpInside)
        if let count = KeychainWrapper.standard.string(forKey: "notiCount") {
            cartButton.badgeBackgroundColor = .red
            cartButton.badge = count
        } else {
            cartButton.badgeBackgroundColor = .clear
            cartButton.badge = ""
        }
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: cartButton)]
    }
    
    @objc func cartButtonTapped() {
        print("cart button pressed")
        self.moveToCartScreen()
    }
    
    func moveToCartScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func orderHistoryAPiCall() {
        userOrder.orderHistoryApiCall(userId: self.userId) { (_isTrue, _message, _data) in
            if _isTrue {
                self.orderData = _data
                DispatchQueue.main.async {
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                }
            } else {
                self.showAlert(title: _message, message: "")
                self.tableView.isHidden = true
            }
        }
    }
    
}

extension MyOrdersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return orderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrdersTVCell", for: indexPath) as! MyOrdersTVCell
        
        cell.orderNoLbl.text = orderData[indexPath.row].orderNo
        cell.dateLbl.text = orderData[indexPath.row].date
        cell.priceLbl.text = orderData[indexPath.row].total
        cell.statusLbl.text = orderData[indexPath.row].status
        cell.paymentStatusLbl.text = orderData[indexPath.row].payStatus
        
        self.addButton(cell: cell, data: orderData[indexPath.row])
        self.addTagForButton(cell: cell, index: indexPath.row)
        
        return cell
    }
    
    func addButton(cell: MyOrdersTVCell, data: OrderHistoryModel) {
        cell.stackView.removeArrangedSubview(cell.viewBtn)
        cell.viewBtn.isHidden = true
        cell.stackView.removeArrangedSubview(cell.pdfBtn)
        cell.pdfBtn.isHidden = true
        cell.stackView.removeArrangedSubview(cell.refundBtn)
        cell.refundBtn.isHidden = true
        cell.stackView.removeArrangedSubview(cell.checkoutBtn)
        cell.checkoutBtn.isHidden = true
        
        let status = data.status
        let paymentStatus = data.payStatus
        
        if status == "pending" && paymentStatus == "open" {
            cell.stackView.addArrangedSubview(cell.viewBtn)
            cell.viewBtn.isHidden = false
            cell.stackView.addArrangedSubview(cell.checkoutBtn)
            cell.checkoutBtn.isHidden = false
        } else if status == "delivered" && paymentStatus == "open" {
            cell.stackView.addArrangedSubview(cell.viewBtn)
            cell.viewBtn.isHidden = false
            cell.stackView.addArrangedSubview(cell.checkoutBtn)
            cell.checkoutBtn.isHidden = false
        } else if status == "pending" && paymentStatus == "paid" {
            cell.stackView.addArrangedSubview(cell.viewBtn)
            cell.viewBtn.isHidden = false
            cell.stackView.addArrangedSubview(cell.pdfBtn)
            cell.pdfBtn.isHidden = false
        } else if status == "delivered" && paymentStatus == "paid" {
            cell.stackView.addArrangedSubview(cell.viewBtn)
            cell.viewBtn.isHidden = false
            cell.stackView.addArrangedSubview(cell.pdfBtn)
            cell.pdfBtn.isHidden = false
            
            if let delivDate = data.deliveryDate {
                if delivDate == "" {
                    //if date is not found then dont add refund
                } else {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy"
                    let currentDate = dateFormatter.date(from: delivDate)
                    var dateCompanent = DateComponents()
                    dateCompanent.day = 14
                    let futureDate = Calendar.current.date(byAdding: dateCompanent, to: currentDate!)
                    if currentDate! < futureDate! {
                        cell.stackView.addArrangedSubview(cell.refundBtn)
                        cell.refundBtn.isHidden = false
                    }
                }
            }
        } else if status == "cancelled" && paymentStatus == "paid" {
            cell.stackView.addArrangedSubview(cell.viewBtn)
            cell.viewBtn.isHidden = false
            cell.stackView.addArrangedSubview(cell.pdfBtn)
            cell.pdfBtn.isHidden = false
        } else {
            cell.stackView.addArrangedSubview(cell.viewBtn)
            cell.viewBtn.isHidden = false
            cell.stackView.addArrangedSubview(cell.pdfBtn)
            cell.pdfBtn.isHidden = false
        }
    }
    
    func addTagForButton(cell: MyOrdersTVCell, index: Int) {
        cell.viewBtn.tag = index
        cell.pdfBtn.tag = index
        cell.checkoutBtn.tag = index
        cell.refundBtn.tag = index
        
        cell.viewBtn.addTarget(self, action: #selector(viewBtnAction(_:)), for: .touchUpInside)
        cell.pdfBtn.addTarget(self, action: #selector(pdfBtnAction(_:)), for: .touchUpInside)
        cell.checkoutBtn.addTarget(self, action: #selector(checkoutBtnAction(_:)), for: .touchUpInside)
        cell.refundBtn.addTarget(self, action: #selector(refundBtnAction(_:)), for: .touchUpInside)
    }
    
    @objc func viewBtnAction(_ sender: UIButton) {
        print("View btn clicked ", sender.tag)
        
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyOrderViewVC") as! MyOrderViewVC
            let id = orderData[sender.tag].id ?? 0
            vc.orderId = String(id)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyOrderViewVC") as! MyOrderViewVC
            let id = orderData[sender.tag].id ?? 0
            vc.orderId = String(id)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @objc func pdfBtnAction(_ sender: UIButton) {
        print("Pdf btn clicked ", sender.tag)
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PDFViewVC") as! PDFViewVC
            vc.pdfStr = orderData[sender.tag].invoicePdf!
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PDFViewVC") as! PDFViewVC
            vc.pdfStr = orderData[sender.tag].invoicePdf!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func checkoutBtnAction(_ sender: UIButton) {
        print("Checkout btn clicked ", sender.tag)
        self.showAlert(title: "Wait!", message: "work in progress")
    }
    
    @objc func refundBtnAction(_ sender: UIButton) {
        print("Refund btn clicked ", sender.tag)
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ReasonRetrunVC") as! ReasonRetrunVC
            vc.orderData = orderData[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReasonRetrunVC") as! ReasonRetrunVC
            vc.orderData = orderData[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
}
