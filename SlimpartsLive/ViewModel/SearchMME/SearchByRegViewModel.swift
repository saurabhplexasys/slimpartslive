//
//  SearchByRegViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 31/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class SearchByRegViewModel {
    
    func searchByReg(regCode: String, completion: @escaping(Bool, String, SearchByRegModel) -> Void) {
        
        AppInstance.showLoader()
        var searchData = SearchByRegModel(makeCode: "", makeName: "", modelCode: "", modelName: "", typeCode: "", typeName: "", kTypeCode: "")
        
        let url = Slimparts.baseURL + Slimparts.searchByRegUrl + regCode
        
        AF.request(url, method: .get).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["ResponseCode"] as? String, let message = responseObj["ResponseText"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem", searchData)
                    return
                }
                if status == "1" {
                    let _data = responseObj["data"] as? [String: Any]
                    let _car = _data!["cars"] as? [String: Any]
                    
                    let _makeCode = _car!["makecode"] as? String
                    let _makeName = _car!["makename"] as? String
                    let _modelCode = _car!["modelcode"] as? String
                    let _modelName = _car!["modelname"] as? String
                    let _typeCode = _car!["typecode"] as? String
                    let _typeName = _car!["typename"] as? String
                    let _kTypeCode = _car!["ktyp"] as? String
                    
                    
                    searchData = SearchByRegModel(makeCode: _makeCode ?? "", makeName: _makeName ?? "", modelCode: _modelCode ?? "", modelName: _modelName ?? "", typeCode: _typeCode ?? "", typeName: _typeName ?? "", kTypeCode: _kTypeCode ?? "")
                    
                    AppInstance.hideLoader()
                    completion(true, message, searchData)
                    
                } else {
                    AppInstance.hideLoader()
                    completion(false, message, searchData)
                }
            case .failure(let error): 
                AppInstance.hideLoader()
                completion(false, "serverProblem", searchData)
            }
        }
        
    }
    
    
}
