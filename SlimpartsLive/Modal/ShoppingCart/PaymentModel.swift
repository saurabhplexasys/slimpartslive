//
//  PaymentModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 22/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct PaymentModel {
    let payId :String?
    let payMethod: String?
    let payStatus: String?
    let payProfileID: String?
    let payOrderNo: String?
    let payCheckOutUrl: String?
}
