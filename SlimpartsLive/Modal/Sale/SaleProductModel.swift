//
//  SaleProductModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 05/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import UIKit

struct SaleProductModel {
    let proId: Int?
    let proUniqId: String?
    let proName: String?
    let proPrice: String?
    let proDesc: String?
    let proImg: String?
    let proQuantity: Int?
    let status: String?
    let catId: Int?
    let subCatId: Int?
    let brand: String?
}
