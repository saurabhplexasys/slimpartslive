//
//  AddCarVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 28/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class AddCarVC: UIViewController, PassPicker {
    
    func passingObject(data: String, code: String, extra: String, staticPicker: String) {
        if staticPicker == "make" {
            // code for make
            self.makeTF.text = data
            self.makeName = data
            self.makeCode = code
            
            self.modelTF.text = "Model"
            self.modelName = ""
            self.modelCode = ""
            
            self.selectEngineTF.text = "Engine Type"
            self.engineName = ""
            self.engineCode = ""
            self.extraKtype = ""
            
        } else if staticPicker == "model" {
            // code for model
            self.modelTF.text = data
            self.modelName = data
            self.modelCode = code
            
            self.selectEngineTF.text = "Engine Type"
            self.engineName = ""
            self.engineCode = ""
            self.extraKtype = ""
            
        } else if staticPicker == "engine" {
            // code for engine
            self.selectEngineTF.text = data
            self.engineName = data
            self.engineCode = code
            self.extraKtype = extra
        }
    }
    
    @IBOutlet weak var makeTF: UITextField!
    @IBOutlet weak var modelTF: UITextField!
    @IBOutlet weak var selectEngineTF: UITextField!
    
    @IBOutlet weak var makeBtnOutlet: UIButton!
    @IBOutlet weak var modelBtnOutlet: UIButton!
    @IBOutlet weak var selectEngineBtnOutlet: UIButton!
    
    var delegatePassData: PassPicker!
    
    var makeUser = MakeViewModel()
    var modelUser = ModelViewModel()
    var engineUser = EngineTypeViewModel()
    var searchUser = SearchKTypeViewModel()
    var searchByReg = SearchByRegViewModel()
    
    var makeName = ""
    var makeCode = ""
    var modelName = ""
    var modelCode = ""
    var engineName = ""
    var engineCode = ""
    var extraKtype = ""
    
    var userId = ""
    var userCar = SaveCarViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func makeBtnAction(sender: UIButton) {
        print("make button pressed")
        DispatchQueue.main.async {
            self.makeAPICall()
        }
        self.modelTF.text = ""
        self.selectEngineTF.text = ""
    }
    
    func makeAPICall() {
        makeUser.getMakeData { (_isTrue, _message, _makeData) in
            if _isTrue {
                let makeList = _makeData.data
                var data = [String]()
                data.removeAll()
                var code = [String]()
                code.removeAll()
                
                for list in makeList! {
                    data.append(list.makename ?? "No Data")
                    code.append(list.makecode ?? "No Data")
                }
                let str = "make"
                self.moveToPickerVC(data: data, code: code, extra: [""], staticString: str)
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    func moveToPickerVC(data: [String], code: [String], extra: [String], staticString: String) {
        if #available(iOS 13.0, *) {
            let pickerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PickerVC") as! PickerVC
            pickerVC.delegatePassData = self
            pickerVC.pickerArrayData = data
            pickerVC.pickerArrayCode = code
            pickerVC.pickerArrayExtra = extra
            pickerVC.staticPicker = staticString
            if staticString == "engine" {
                pickerVC.extraBool = true
            } else {
                pickerVC.extraBool = false
            }
            self.present(pickerVC, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let pickerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PickerVC") as! PickerVC
            pickerVC.delegatePassData = self
            pickerVC.pickerArrayData = data
            pickerVC.pickerArrayCode = code
            pickerVC.pickerArrayExtra = extra
            pickerVC.staticPicker = staticString
            if staticString == "engine" {
                pickerVC.extraBool = true
            } else {
                pickerVC.extraBool = false
            }
            self.present(pickerVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func modelBtnAction(sender: UIButton) {
        print("model button pressed")
        if makeTF.text != nil {
            DispatchQueue.main.async {
                self.modelAPICall()
            }
        } else {
            self.showAlert(title: "Please Select Make", message: "")
        }
        self.selectEngineTF.text = ""
    }
    
    func modelAPICall() {
        modelUser.getModelData(makeCode: makeCode) { (_isTrue, _message, _modelData) in
            if _isTrue {
                let modelList = _modelData
                var data = [String]()
                data.removeAll()
                var code = [String]()
                code.removeAll()
                
                for list in modelList {
                    data.append(list.modelName ?? "No Name")
                    code.append(list.modelCode ?? "No Code")
                }
                let str = "model"
                self.moveToPickerVC(data: data, code: code, extra: [""], staticString: str)
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    @IBAction func selectEngineBtnAction(sender: UIButton) {
        print("engine type button pressed")
        if modelTF.text != nil {
            DispatchQueue.main.async {
                self.engineTypeApiCall()
            }
        } else {
            self.showAlert(title: "Please Select Model", message: "")
        }
    }
    
    func engineTypeApiCall() {
        engineUser.getEngineTypeData(modelCode: modelCode) { (_isTrue, _message, _engineData) in
            if _isTrue {
                let engineList = _engineData
                var data = [String]()
                data.removeAll()
                var code = [String]()
                code.removeAll()
                var ktype = [String]()
                ktype.removeAll()
                
                for list in engineList {
                    data.append(list.typeName ?? "No Name")
                    code.append(list.typeCode ?? "No Code")
                    ktype.append(list.kTypeCode ?? "No data")
                }
                let str = "engine"
                self.moveToPickerVC(data: data, code: code, extra: ktype, staticString: str)
                
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    @IBAction func saveBtnAction(sender: UIButton) {
        print("save btn clicked")
        if self.makeTF.text!.count > 0 {
            if self.modelTF.text!.count > 0 {
                if self.selectEngineTF.text!.count > 0 {
                    self.saveCarApiCall()
                } else {
                    self.showAlert(title: "Alert", message: "Please select an engine type")
                }
            } else {
                self.showAlert(title: "Alert", message: "Please select model")
            }
        } else {
            self.showAlert(title: "Alert", message: "Please select make")
        }
    }
    
    func saveCarApiCall() {
        userCar.saveCarsApiCall(kType: self.extraKtype, make: self.makeName, model: self.modelName, engineType: self.engineName, userId: self.userId) { (_isTrue, _message) in
            if _isTrue {
                self.alertController(title: _message, message: "")
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
    }
    
    func alertController(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAct = UIAlertAction(title: "OK", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(okAct)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
