//
//  PDFViewVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 02/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import PDFKit
import WebKit

class PDFViewVC: UIViewController, WKNavigationDelegate {

    var pdfStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.navigationItem.title = "PDF"
        
        let webview = WKWebView(frame: UIScreen.main.bounds)
        view.addSubview(webview)
        webview.navigationDelegate = self
        if pdfStr == "" {
            self.moveBack()
        } else {
            webview.load(URLRequest(url: URL(string: pdfStr)!))
        }
        
        
    }
    
    func moveBack() {
        let alert = UIAlertController(title: "PDF Not Found!", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    


}
