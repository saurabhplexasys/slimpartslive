//
//  MakeViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 29/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class MakeViewModel {
    
    func getMakeData(completion : @escaping(Bool, String, MakeModel) -> Void) {
        
        AppInstance.showLoader()
        
        let header: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        var makeData : MakeModel?
        
        let url = Slimparts.baseURL + Slimparts.makeURL
                
        AF.request(url, method: .get, headers: header).response { (response) in
            if let data = response.data {
                do {
                    let userRes = try JSONDecoder().decode(MakeModel.self, from: data)
                    makeData = userRes
                    if makeData?.responseCode == "1" {
                        AppInstance.hideLoader()
                        completion(true, makeData?.responseText ?? "", makeData!)
                    } else {
                        AppInstance.hideLoader()
                        completion(false, makeData?.responseText ?? "", makeData!)
                    }
                } catch let err {
                    AppInstance.hideLoader()
                    completion(false, err.localizedDescription, makeData!)
                }
            } else {
                AppInstance.hideLoader()
                completion(false, "", makeData!)
            }
        }
        
    }
    
    
}
