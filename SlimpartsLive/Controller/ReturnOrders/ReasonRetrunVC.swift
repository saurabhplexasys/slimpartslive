//
//  ReasonRetrunVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 03/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class ReasonRetrunVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtnOutlet: UIButton!
    
    var reasonArr = ["The items received do not match the items I ordered", "The ordered parts do not fit my car", "I no longer need the ordered parts", "The parts and / or package are damaged", "Other reasons"]
    
    var orderData: OrderHistoryModel?
    
    var selectedIndexPath: NSIndexPath? = NSIndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Refund"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        if reasonArr[selectedIndexPath!.row] == "Other reasons" {
            self.showAlert(title: "Alert", message: "Please select reason from first four reason")
        } else {
            self.goToProductsScreen(index: selectedIndexPath!.row)
        }
    }
    

}

extension ReasonRetrunVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reasonArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReasonReturnTVCell", for: indexPath) as! ReasonReturnTVCell
        
        if indexPath == selectedIndexPath! as IndexPath {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        
        cell.titleLbl.text = reasonArr[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath == selectedIndexPath! as IndexPath {
            return
        }

        // toggle old one off and the new one on
        let newCell = tableView.cellForRow(at: indexPath)
        if newCell?.accessoryType == UITableViewCell.AccessoryType.none {
            newCell?.accessoryType = UITableViewCell.AccessoryType.checkmark
        }
        let oldCell = tableView.cellForRow(at: selectedIndexPath! as IndexPath)
        if oldCell?.accessoryType == UITableViewCell.AccessoryType.checkmark {
            oldCell?.accessoryType = UITableViewCell.AccessoryType.none
        }

        selectedIndexPath = indexPath as NSIndexPath  // save the selected index path
        
        if reasonArr[selectedIndexPath!.row] == "Other reasons" {
            self.goToOtherOptionScreen()
        } else {
            print("selected from first four")
        }
        
    }
    
    func goToProductsScreen(index: Int) {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "RefundProductsVC") as! RefundProductsVC
            vc.orderData = orderData
            vc.selectedReason = reasonArr[index]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RefundProductsVC") as! RefundProductsVC
            vc.orderData = orderData
            vc.selectedReason = reasonArr[index]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func goToOtherOptionScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ReturnPartsVC") as! ReturnPartsVC
            vc.orderData = orderData
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReturnPartsVC") as! ReturnPartsVC
            vc.orderData = orderData
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
