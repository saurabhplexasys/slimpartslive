//
//  LoginAsGuestVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 24/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class ProfileVC: UIViewController {

    @IBOutlet weak var emailTFCD: UITextField!
    @IBOutlet weak var firstNameTFCD: UITextField!
    @IBOutlet weak var lastNameTFCD: UITextField!
    @IBOutlet weak var mobileNoTFCD: UITextField!
    
    @IBOutlet weak var countryTFBD: UITextField!
    @IBOutlet weak var houseNoTFBD: UITextField!
    @IBOutlet weak var addressTFBD: UITextField!
    @IBOutlet weak var cityTFBD: UITextField!
    @IBOutlet weak var psotCodeTFBD: UITextField!
    
    @IBOutlet weak var countryTFSD: UITextField!
    @IBOutlet weak var houseNoTFSD: UITextField!
    @IBOutlet weak var addressTFSD: UITextField!
    @IBOutlet weak var cityTFSD: UITextField!
    @IBOutlet weak var psotCodeTFSD: UITextField!
    
    @IBOutlet weak var sameAsBillBtnOutlet: UIButton!
    @IBOutlet weak var updateProfileBtnOutlet: UIButton!
    @IBOutlet weak var updateAddressBtnOutlet: UIButton!
    @IBOutlet weak var countryBDBtnOutlet: UIButton!
    @IBOutlet weak var countrySDBtnOutlet: UIButton!
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    
    
    var userId = ""
    var userProfile = ProfileViewModel()
    var userProfileData : ProfileModel?
    
    var getDetails = UserDetailsViewModel()
    var userDetails: UserDetailsModel?
    
    var userUpdateAdd = ProfileAddressUpdateViewModel()
    
    var iconClick = true {
        didSet {
            if iconClick {
                let image = UIImage(named: "rightTick")
                sameAsBillBtnOutlet.setImage(image, for: .normal)
                self.addressTFSD.text = self.addressTFBD.text
                self.cityTFSD.text = self.cityTFBD.text
                self.psotCodeTFSD.text = self.psotCodeTFBD.text
                self.houseNoTFSD.text = self.houseNoTFBD.text
            } else {
                let image = UIImage(named: "")
                sameAsBillBtnOutlet.setImage(image, for: .normal)
                self.addressTFSD.text = ""
                self.cityTFSD.text = ""
                self.psotCodeTFSD.text = ""
                self.houseNoTFSD.text = ""
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.navigationController?.navigationBar.tintColor = .white
        self.setMenuBarBtn()
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
            print("user id:- ", self.userId)
        }
        self.updateProfilePersonalDetails()
        self.getUserDetailsAPiCall()
        // Do any additional setup after loading the view.
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func getUserDetailsAPiCall() {
        getDetails.getUserDatailsAPI(userId: userId) { (_isTrue, _message, _userData) in
            if _isTrue {
                self.userDetails = _userData
                DispatchQueue.main.async {
                    self.updateProfileAfterGetUserDetialsAPI()
                    self.updateAddressAfterGetUserDetailsAPI()
                }
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    func updateProfileAfterGetUserDetialsAPI() {
        let userName = (userDetails?.firstName ?? "") + " " + (userDetails?.lastName ?? "")
        KeychainWrapper.standard.set(userName, forKey: "userName")
        KeychainWrapper.standard.set(userDetails?.email ?? "", forKey: "email")
        KeychainWrapper.standard.set(userDetails?.mobile ?? "", forKey: "mobile")
    }
    
    func updateAddressAfterGetUserDetailsAPI() {
        if !(userDetails?.addressDetail!.isEmpty)! {
            for detail in (userDetails?.addressDetail)! {
                let addType = detail.addressType
                if addType == "billing" {
                    self.addressTFBD.text = detail.address
                    self.cityTFBD.text = detail.city
                    self.psotCodeTFBD.text = detail.postCode
                    self.houseNoTFBD.text = detail.houseNo
                } else {
                    self.addressTFSD.text = detail.address
                    self.cityTFSD.text = detail.city
                    self.psotCodeTFSD.text = detail.postCode
                    self.houseNoTFSD.text = detail.houseNo
                }
            }
        }
    }
    
    func updateProfilePersonalDetails() {
        
        if let userName = KeychainWrapper.standard.string(forKey: "userName") {
            let fullName    = userName
            let fullNameArr = fullName.components(separatedBy: " ")

            let name    = fullNameArr[0]
            let surname = fullNameArr[1]
            self.firstNameTFCD.text = name
            self.lastNameTFCD.text = surname
        }
        if let email = KeychainWrapper.standard.string(forKey: "email") {
            self.emailTFCD.text = email
        }
        if let mobile = KeychainWrapper.standard.string(forKey: "mobile") {
            self.mobileNoTFCD.text = mobile
        }
        
    }
    
    @IBAction func countryBillingBtnAction(sender: UIButton) {
        print("country billing btn pressed")
    }
    
    @IBAction func countryShippingBtnAction(sender: UIButton) {
        print("country shipping btn pressed")
    }
    
    @IBAction func sameAsbillBtnAction(sender: UIButton) {
        print("same as billing btn pressed")
        iconClick = !iconClick
        UIView.animate(withDuration: 0, delay: 0, options: .transitionCrossDissolve, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) { (success) in
            sender.isSelected = !sender.isSelected
            UIView.animate(withDuration: 0, delay: 0, options: .transitionCrossDissolve, animations: {
                sender.transform = .identity
            }, completion: nil)
        }
    }
    
    @IBAction func updateProfileBtnAction(sender: UIButton) {
        print("update profile btn pressed")
        
        if firstNameTFCD.text!.count > 0 {
            if lastNameTFCD.text!.count > 0 {
                if mobileNoTFCD.text!.count > 0 {
                    self.updateProfileApiCall()
                } else {
                    self.showAlert(title: "Alert", message: "enter your mobile number")
                }
            } else {
                self.showAlert(title: "Alert", message: "enter your last name")
            }
        } else {
            self.showAlert(title: "Alert", message: "enter your first name")
        }
    }
    
    func updateProfileApiCall() {
        userProfile.updateProfileAPI(firstName: firstNameTFCD.text ?? "", lastName: lastNameTFCD.text ?? "", mobile: mobileNoTFCD.text ?? "", userId: userId) { (_isTrue, _message, _profileData) in
            if _isTrue {
                self.userProfileData = _profileData
                DispatchQueue.main.async {
                    self.updateKeychainProfile(userData: self.userProfileData!)
                }
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    func updateKeychainProfile(userData: ProfileModel) {
        let data = userData
        let userName = (data.firstName ?? "") + " " + (data.lastName ?? "")
        KeychainWrapper.standard.set(userName, forKey: "userName")
        KeychainWrapper.standard.set(data.mobile ?? "", forKey: "mobile")
        self.updateProfilePersonalDetails()
    }
    
    
    @IBAction func updateAddressBtnAction(sender: UIButton) {
        print("update address btn pressed")
        self.addressValidation()
    }
    
    func addressValidation() {
        if houseNoTFBD.text!.count > 0 {
            if addressTFBD.text!.count > 0 {
                if cityTFBD.text!.count > 0 {
                    if psotCodeTFBD.text!.count > 0 {
                        if houseNoTFSD.text!.count > 0 {
                            if addressTFSD.text!.count > 0 || cityTFSD.text!.count > 0 || psotCodeTFSD.text!.count > 0 {
                                if addressTFSD.text!.count > 0 {
                                    if cityTFSD.text!.count > 0 {
                                        if psotCodeTFSD.text!.count > 0 {
                                            self.updateAddressAPiCall()
                                        } else {
                                            self.showAlert(title: "Alert", message: "please enter shipping postcode")
                                        }
                                    } else {
                                        self.showAlert(title: "Alert", message: "please enter shipping town/city")
                                    }
                                } else {
                                    self.showAlert(title: "Alert", message: "please enter shipping address")
                                }
                            } else {
                                self.updateAddressAPiCall()
                            }
                        } else {
                            self.showAlert(title: "Alert", message: "please enter shipping house no")
                        }
                    } else {
                        self.showAlert(title: "Alert", message: "please enter billing postcode")
                    }
                } else {
                    self.showAlert(title: "Alert", message: "please enter billing town/city")
                }
            } else {
                self.showAlert(title: "Alert", message: "please enter billing address")
            }

        } else {
            self.showAlert(title: "Alert", message: "please enter billing house no")
        }
    }
    
    
    func updateAddressAPiCall() {
        
        let houseNoBD = self.houseNoTFBD.text ?? ""
        let houseNoSD = self.houseNoTFSD.text ?? ""
        
        var addressBD = self.addressTFBD.text!
        var addressSD = self.addressTFSD.text!
        
        if houseNoBD.count > 0 {
            let add = "\(houseNoBD), \(addressBD)"
            addressBD = add
        }
        if houseNoSD.count > 0 {
            let add = "\(houseNoSD), \(addressSD)"
            addressSD = add
        }
        userUpdateAdd.updateAddressAPiCall(addressB: addressBD, cityB: cityTFBD.text ?? "", postcodeB: psotCodeTFBD.text ?? "", addressS: addressSD, cityS: cityTFSD.text ?? "", postcodeS: psotCodeTFSD.text ?? "", userId: userId) { (_isTrue, _message) in
            if _isTrue {
                self.showAlert(title: _message, message: "")
                self.getUserDetailsAPiCall()
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    
    
    
    
    

}
