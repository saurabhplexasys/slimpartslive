//
//  MakeModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 29/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct MakeModel : Codable {
    let responseCode : String?
    let responseText : String?
    let data : [MakeData]?

    enum CodingKeys: String, CodingKey {

        case responseCode = "ResponseCode"
        case responseText = "ResponseText"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        responseCode = try values.decodeIfPresent(String.self, forKey: .responseCode)
        responseText = try values.decodeIfPresent(String.self, forKey: .responseText)
        data = try values.decodeIfPresent([MakeData].self, forKey: .data)
    }

}

struct MakeData : Codable {
    let makecode : String?
    let makename : String?

    enum CodingKeys: String, CodingKey {

        case makecode = "makecode"
        case makename = "makename"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        makecode = try values.decodeIfPresent(String.self, forKey: .makecode)
        makename = try values.decodeIfPresent(String.self, forKey: .makename)
    }

}
