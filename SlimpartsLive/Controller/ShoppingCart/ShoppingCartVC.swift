//
//  ShoppingCartVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 13/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class ShoppingCartVC: UIViewController {

    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    
    @IBOutlet weak var subTotTitleLbl: UILabel!
    @IBOutlet weak var taxTitleLbl: UILabel!
    @IBOutlet weak var shippingTitleLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    
    @IBOutlet weak var checkOutBtn: UIButton!
    
    @IBOutlet weak var priceDetailsStackView: UIStackView!
    
    var cartUser = ShoppingCartListViewModel()
    var cartList: ShoppingCartModel?
    var quanUser = UpdateCartQuantityViewModel()
    var deleteUser = DeleteCartItemViewModel()
    var userId = ""
    
    let cartButton = SSBadgeButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.setMenuBarBtn()
        self.cartBtnView()
        self.navigationController?.navigationBar.tintColor = .white
        self.priceDetailsStackView.isHidden = true
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            userId = userid
            self.cartListAPICall()
        } else {
            self.checkOutBtn.isHidden = true
            self.showAlertAction(title: "Please add item in cart", message: "", alertStyle: .alert, actionTitles: ["Ok"], actionStyles: [.default], actions: [{_ in
                self.navigationController?.popViewController(animated: true)
            }])
        }
        
        // Do any additional setup after loading the view.
    }
    
    func cartBtnView() {
        cartButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        cartButton.setImage(UIImage(named: "cartIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cartButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        cartButton.tintColor = .white
        
        if let count = KeychainWrapper.standard.string(forKey: "notiCount") {
            cartButton.badgeBackgroundColor = .red
            cartButton.badge = count
        } else {
            cartButton.badgeBackgroundColor = .clear
            cartButton.badge = ""
        }
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: cartButton)]
    }
    
    func updateCartBadge() {
        if let count = KeychainWrapper.standard.string(forKey: "notiCount") {
            cartButton.badgeBackgroundColor = .red
            cartButton.badge = count
        } else {
            cartButton.badgeBackgroundColor = .clear
            cartButton.badge = ""
        }
    }
    
    func updatePriceDetails() {
        self.subTotalLbl.text = cartList?.subtotal ?? ""
        self.taxLbl.text = cartList?.tax ?? ""
        self.shippingLbl.text = cartList?.shipping ?? ""
        self.totalLbl.text = cartList?.total ?? ""
    }
    
    
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func cartListAPICall() {
        cartUser.cartListApiCall(userId: userId) { (_isTrue, _message, _list) in
            if _isTrue {
                self.cartList = _list
                DispatchQueue.main.async {
                    if (self.cartList?.cartList.isEmpty)! {
                        self.priceDetailsStackView.isHidden = true
                    } else {
                        self.priceDetailsStackView.isHidden = false
                        self.updatePriceDetails()
                    }
                    self.updateCartBadge()
                    self.tableView.reloadData()
                }
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ShoppingCartVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartList?.cartList.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingCartTVCell", for: indexPath) as! ShoppingCartTVCell
        
        cell.itemLbl.text = cartList?.cartList[indexPath.row].name
        cell.priceLbl.text = cartList?.cartList[indexPath.row].price
        cell.totalPriceLbl.text = cartList?.cartList[indexPath.row].totalPrice
        
        let quantityStr = cartList?.cartList[indexPath.row].count
        cell.quantityTf.text = String(quantityStr ?? 0)
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteAction(_:)), for: .touchUpInside)
        
         //quantity increase and decrease
        if let quantity = cartList?.cartList[indexPath.row].totQuan {
            if quantity > 0 {
                
                cell.minusBtn.tag = indexPath.row
                cell.plusBtn.tag = indexPath.row
                cell.minusBtn.addTarget(self, action: #selector(minusBtnAction(_:)), for: .touchUpInside)
                cell.plusBtn.addTarget(self, action: #selector(plusBtnAction(_:)), for: .touchUpInside)
            } else {
                
            }
        }
        
        
        return cell
    }
    
    @objc func minusBtnAction(_ sender: UIButton) {
        
        print("minus btn press")
        let quantity = cartList?.cartList[sender.tag].totQuan
        let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! ShoppingCartTVCell
        let quaTxt = Int(cell.quantityTf.text ?? "1")
        if quaTxt == 1 {
            cell.quantityTf.text = "1"
        } else if quaTxt! >= 1 || quaTxt! <= quantity ?? 1{
            cell.quantityTf.text = String(quaTxt! - 1)
            let cartId = cartList?.cartList[sender.tag].id
            self.updateCartQuantityAPICall(userId: self.userId, cartId: String(cartId ?? 0), quantity: cell.quantityTf.text ?? "1")
        } else {
            cell.quantityTf.text = "1"
        }
    }
    
    @objc func plusBtnAction(_ sender: UIButton) {
        
        print("plus btn press")
        let quantity = cartList?.cartList[sender.tag].totQuan
        let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! ShoppingCartTVCell
        let quaTxt = Int(cell.quantityTf.text ?? "1")
        if quaTxt! <= quantity ?? 1{
            if quaTxt == quantity {
                self.showAlert(title: "You can not add more than \(quaTxt ?? 0) units", message: "")
            } else {
                let out = (quaTxt! + 1)
                cell.quantityTf.text = String(out)
                let cartId = cartList?.cartList[sender.tag].id
                self.updateCartQuantityAPICall(userId: self.userId, cartId: String(cartId ?? 0), quantity: cell.quantityTf.text ?? "1")
            }
        } else {
            cell.quantityTf.text = "1"
        }
    }
    
    func updateCartQuantityAPICall(userId: String, cartId: String, quantity: String) {
        quanUser.updateCartQuantityAPICall(userId: userId, cartId: cartId, quantity: quantity) { (_isTrue, _message) in
            if _isTrue {
                print("Success update cart item")
                self.cartListAPICall()
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
    }
    
    @objc func deleteAction(_ sender: UIButton) {
        print("delete cart item clicked row:- ", sender.tag)
        let cartId = cartList?.cartList[sender.tag].id
        self.deleteCartItemApiCall(userId: self.userId, cartId: String(cartId ?? 0))
        
    }
    
    func deleteCartItemApiCall(userId: String, cartId: String) {
        deleteUser.deleteCartItemApiCall(userId: userId, cartId: cartId) { (_isTrue, _message) in
            if _isTrue {
                print("Cart item deleted successfully")
                DispatchQueue.main.async {
                    self.updateCartBadge()
                    self.cartListAPICall()
                }
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
    }
    
    @IBAction func checkOutAction(_ sender: UIButton) {
        
        if (cartList?.cartList.isEmpty)! {
            self.showAlert(title: "Please add item in cart", message: "")
        } else {
            // go for checkout
            print("you can go for checkout")
            if #available(iOS 13.0, *) {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CheckoutUserAddressVC") as! CheckoutUserAddressVC
                vc.cartList = self.cartList
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                // Fallback on earlier versions
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckoutUserAddressVC") as! CheckoutUserAddressVC
                vc.cartList = self.cartList
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    
    
    
    
    
}
