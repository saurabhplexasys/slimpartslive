//
//  CarListModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 02/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct CarListModel {
    let carId: Int?
    let make: String?
    let model: String?
    let engineType: String?
    let typeCode: String?
}
