//
//  MenuTVC.swift
//  Slimparts
//
//  Created by Dharmesh Kothari on 14/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class MenuTVC: UITableViewController {
    
    var titleGuestLogin = ["MY ACCOUNT", "HOME", "DISC BRAKES", "ENGINE OIL", "BATTERIES", "SALE"]
    var titleWithLogin = ["HOME", "DISC BRAKES", "ENGINE OIL", "BATTERIES", "SALE", "LOGOUT"]
    var onClickTitleCell = ["MY ORDERS", "PROFILE", "CHANGE PASSWORD", "MY CARS", "HOME", "DISC BRAKES", "ENGINE OIL", "BATTERIES", "SALE", "LOGOUT"]
    
    var isGuestLogin: Bool = false
    var isUserLogin: Bool = false
    var isUserLoginNameClick: Bool = false
    
    var loginStatus: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (KeychainWrapper.standard.string(forKey: "userID") != nil) {
            isUserLogin = true
            isGuestLogin = false
            loginStatus = true
            if let userName = KeychainWrapper.standard.string(forKey: "userName") {
                titleWithLogin.insert("Hi, \(userName)", at: 0)
                onClickTitleCell.insert("Hi, \(userName)", at: 0)
            } else {
                titleWithLogin.insert("Hi, ", at: 0)
                onClickTitleCell.insert("Hi, ", at: 0)
            }
        } else {
            isUserLogin = false
            isGuestLogin = true
            loginStatus = false
        }
        
        self.tableView.reloadData()
          
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (KeychainWrapper.standard.string(forKey: "userID") != nil) {
            if loginStatus == true {
                
            } else {
                isUserLogin = true
                loginStatus = true
                isGuestLogin = false
                if let userName = KeychainWrapper.standard.string(forKey: "userName") {
                    titleWithLogin.insert("Hi, \(userName)", at: 0)
                    onClickTitleCell.insert("Hi, \(userName)", at: 0)
                } else {
                    titleWithLogin.insert("Hi, ", at: 0)
                    onClickTitleCell.insert("Hi, ", at: 0)
                }
            }
        } else {
            isUserLogin = false
            loginStatus = false
            isGuestLogin = true
        }
        self.tableView.reloadData()
        
    }
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isGuestLogin == true {
            return titleGuestLogin.count
        } else if isUserLogin == true {
            return titleWithLogin.count
        } else if isUserLoginNameClick == true {
            return onClickTitleCell.count
        } else {
            return titleGuestLogin.count
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTVCell", for: indexPath) as! MenuTVCell
        
        if isGuestLogin == true {
            cell.titleLbl.text = titleGuestLogin[indexPath.row]
        } else if isUserLogin == true {
            cell.titleLbl.text = titleWithLogin[indexPath.row]
        } else if isUserLoginNameClick == true {
            cell.titleLbl.text = onClickTitleCell[indexPath.row]
        } else {
            cell.titleLbl.text = titleGuestLogin[indexPath.row]
        }
        
        if loginStatus == true {
            if indexPath.row == 0 {
                cell.img.image = UIImage(named: "AppIcon")
                cell.imgHeighContraint.constant = 44
                cell.imgWidthContraint.constant = 44
            } else {
                cell.img.image = UIImage(named: "menuImage")
                cell.imgHeighContraint.constant = 30
                cell.imgWidthContraint.constant = 30
            }
        } else {
            cell.img.image = UIImage(named: "menuImage")
            cell.imgHeighContraint.constant = 30
            cell.imgWidthContraint.constant = 30
        }
        
        cell.img.layer.masksToBounds = false
        cell.img.layer.cornerRadius = cell.img.frame.height / 2.0
        cell.img.clipsToBounds = true
        
        if isUserLoginNameClick == true {
            if indexPath.row == 0 {
                cell.arrowImg.image = UIImage(named: "arrowDown")
            } else {
                cell.arrowImg.image = UIImage(named: "arrowRight")
            }
            
            if (indexPath.row > 0 && indexPath.row <= 4) {
                cell.backgroundColor = UIColor(displayP3Red: 242/255, green: 242/255, blue: 247/255, alpha: 1)
                cell.titleLbl.textColor = .darkGray
                cell.arrowImg.image = UIImage(named: "arrowRightGray")
                cell.img.image = UIImage(named: "")
            } else {
                cell.backgroundColor = .white
                cell.titleLbl.textColor = UIColor(displayP3Red: 00/255, green: 54/255, blue: 93/255, alpha: 1)
                //cell.arrowImg.image = UIImage(named: "arrowRight")
            }
            
        } else {
            cell.arrowImg.image = UIImage(named: "arrowRight")
            cell.backgroundColor = .white
        }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if loginStatus == true {
            if indexPath.row == 0 {
                return 80
            } else {
                return 60
            }
        } else {
            return 60
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var identifier = ""
        
        if isGuestLogin == true {
            identifier = titleGuestLogin[indexPath.row]
            moveForDiscEngineOilBattery(identifier: identifier)
        } else if isUserLogin == true {
            identifier = titleWithLogin[indexPath.row]
            if indexPath.row == 0 {
                isGuestLogin = false
                isUserLogin = false
                isUserLoginNameClick = true
                self.tableView.reloadData()
            } else if identifier == "LOGOUT" {
                self.logoutApp()
            } else {
                moveForDiscEngineOilBattery(identifier: identifier)
            }
        } else if isUserLoginNameClick == true {
            identifier = onClickTitleCell[indexPath.row]
            if indexPath.row == 0 {
                isGuestLogin = false
                isUserLogin = true
                isUserLoginNameClick = false
                self.tableView.reloadData()
            } else if identifier == "LOGOUT" {
                self.logoutApp()
            } else {
                moveForDiscEngineOilBattery(identifier: identifier)
            }
        } else {
            if identifier == "LOGOUT" {
                self.logoutApp()
            } else {
                identifier = titleGuestLogin[indexPath.row]
                moveForDiscEngineOilBattery(identifier: identifier)
            }
        }
         
    }
    
    func moveForDiscEngineOilBattery(identifier: String) {
        
        if identifier == "DISC BRAKES" || identifier == "ENGINE OIL" || identifier == "BATTERIES" {
            if let code = KeychainWrapper.standard.string(forKey: "typeCode") {
                if identifier == "DISC BRAKES" {
                    KeychainWrapper.standard.set("29", forKey: "menuCode")
                } else if identifier == "ENGINE OIL" {
                    KeychainWrapper.standard.set("48", forKey: "menuCode")
                } else if identifier == "BATTERIES" {
                    KeychainWrapper.standard.set("4", forKey: "menuCode")
                } else {
                    KeychainWrapper.standard.removeObject(forKey: "menuCode")
                }
                if code == "" {
                    self.performSegue(withIdentifier: "HOME", sender: self)
                } else {
                    // go to category screen
                    self.performSegue(withIdentifier: "AllCategory", sender: self)
                }
            } else {
                self.performSegue(withIdentifier: "HOME", sender: self)
            }
        } else {
            self.performSegue(withIdentifier: identifier, sender: self)
        }
        
    }
    
    func logoutApp() {
        let alertCont = UIAlertController(title: "Log Out", message: "Are you sure you want to logout?", preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
            KeychainWrapper.standard.removeObject(forKey: "userID")
            KeychainWrapper.standard.removeObject(forKey: "userName")
            KeychainWrapper.standard.removeObject(forKey: "email")
            KeychainWrapper.standard.removeObject(forKey: "mobile")
            KeychainWrapper.standard.removeObject(forKey: "notiCount")
            KeychainWrapper.standard.removeObject(forKey: "typeCode")
            KeychainWrapper.standard.removeObject(forKey: "menuCode")
            KeychainWrapper.standard.removeObject(forKey: "makeName")
            KeychainWrapper.standard.removeObject(forKey: "modelName")
            KeychainWrapper.standard.removeObject(forKey: "engineName")
            
            self.isGuestLogin = true
            self.loginStatus = false
            self.isUserLogin = false
            self.isUserLoginNameClick = false
            self.tableView.reloadData()
            self.performSegue(withIdentifier: "MY ACCOUNT", sender: self)
        }
        
        alertCont.addAction(cancel)
        alertCont.addAction(okAction)
        
        self.present(alertCont, animated: true, completion: nil)
    }
    
}
