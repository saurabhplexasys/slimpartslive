//
//  AccessoriesViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 04/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class SaleViewModel {
    
    func getAccessoriesListApi(completion: @escaping(Bool, String, [SaleModel]) -> Void) {
        
        AppInstance.showLoader()
        var accData = [SaleModel]()
        accData.removeAll()
        
        
        let url = Slimparts.baseURL + Slimparts.getCategoryAndSubcatURL
        
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
            case .success:
                
                let responseObj = response.value as? [[String: Any]]
                if !responseObj!.isEmpty {
                    
                    for respo in responseObj! {
                        
                        let _catId = respo["id"] as? Int
                        let _catName = respo["name"] as? String
                        let _subCat = respo["subcat"] as? [[String: Any]]
                        
                        var subCatData = [SubCategoryAccessories]()
                        subCatData.removeAll()
                        
                        if _subCat != nil {
                            
                            for sub in _subCat! {
                                let _subCatId = sub["id"] as? Int
                                let _subCatName = sub["name"] as? String
                                
                                let data = SubCategoryAccessories(subCatId: _subCatId ?? 0, subCatName: _subCatName ?? "")
                                subCatData.append(data)
                                
                            }
                            let dataCat = SaleModel(catId: _catId ?? 0, catName: _catName ?? "", subCat: subCatData)
                            accData.append(dataCat)
                        } else {
                            // if subCat not found then dont add to list
                        }
                        
                    }
                    AppInstance.hideLoader()
                    completion(true, "", accData)
                    
                } else {
                    AppInstance.hideLoader()
                    completion(false, "No Data Found", accData)
                }
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! try after some time", accData)
            }
            
        }
        
    }
    
    
    
}
