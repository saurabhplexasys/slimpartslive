//
//  PickerVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 30/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

protocol PassPicker {
    func passingObject(data: String, code: String, extra: String, staticPicker: String)
}

class PickerVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerView: UIPickerView!
    
    var selectedData = ""
    var selectedCode = ""
    var extra = ""
    
    var extraBool = false
    
    var pickerArrayData = [String]()
    var pickerArrayCode = [String]()
    var pickerArrayExtra = [String]()
    
    var staticPicker = ""
    
    var normalPicker : Bool = false
    
    var delegatePassData: PassPicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pickerView.delegate = self
        pickerView.dataSource = self
        
        self.selectedData = pickerArrayData[0]
        
        self.selectedCode = pickerArrayCode[0]
        
        if extraBool == true {
            self.extra = pickerArrayExtra[0]
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneBtnAction(_ sender: Any) {
        if selectedData.count == 0 {
            self.alert(title: "please select at least one from the list", mes: "", delegate: self)
        } else {
            dismiss(animated: true) {
                self.delegatePassData.passingObject(data: self.selectedData, code: self.selectedCode, extra: self.extra, staticPicker: self.staticPicker)
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArrayData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArrayData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedData = pickerArrayData[row]
        selectedCode = pickerArrayCode[row]
        if extraBool == true  {
            extra = pickerArrayExtra[row]
        }
        
    }
    
    func alert(title: String, mes: String, delegate: AnyObject) {
        let alert = UIAlertController(title: title, message: mes, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okBtn)
        self.present(alert, animated: true, completion: nil)
    }
    
}
