//
//  UserCartListViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 13/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

class ShoppingCartListViewModel {
    
    func cartListApiCall(userId: String, completion: @escaping(Bool, String, ShoppingCartModel) -> Void) {
        
        AppInstance.showLoader()
        let url = Slimparts.baseURL + Slimparts.cartListURL + userId
        
        var cartArr = [CartListSCM]()
        cartArr.removeAll()
        var listSCM = ShoppingCartModel(tax: "", subtotal: "", shipping: "", total: "", notiCount: 0, cartList: cartArr)
        
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                guard let responseObj = response.value as? [String: Any] else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! try after some time", listSCM)
                    return
                }
                let cartList = responseObj["cartlist"] as? [[String: Any]]
                let _tax = responseObj["tax"] as? String
                let _subtotal = responseObj["subtotal"] as? String
                let _shipping = responseObj["shipping"] as? String
                let _total = responseObj["total"] as? String
                let _count = responseObj["count"] as? Int
                
                if !cartList!.isEmpty {
                    
                    for cart in cartList! {
                        
                        let _id = cart["id"] as? Int
                        let _totQuant = cart["total_quantity"] as? Int
                        let _name = cart["name"] as? String
                        let _price = cart["price"] as? String
                        let _totalPrice = cart["total"] as? String
                        let _count = cart["count"] as? Int
                        let _image = cart["image"] as? String
                        let _vehicleName = cart["vehicleName"] as? String
                        let _marginPrice = cart["margin_price"] as? Double
                        let _code = cart["code"] as? String
                        let _supcode = cart["supcode"] as? String
                        let _btw = cart["btw"] as? String
                        let _extraFee = cart["extrafees"] as? Double
                        let data = CartListSCM(id: _id ?? 0, totQuan: _totQuant ?? 0, name: _name ?? "", price: _price ?? "", totalPrice: _totalPrice ?? "", count: _count ?? 0, image: _image ?? "", vehicleName: _vehicleName ?? "", marginPrice: _marginPrice ?? 0.0, code: _code ?? "", supcode: _supcode ?? "", btw: _btw ?? "", extraFee: _extraFee ?? 0.0)
                        cartArr.append(data)
                    }
                    
                    listSCM = ShoppingCartModel(tax: _tax ?? "", subtotal: _subtotal ?? "", shipping: _shipping ?? "", total: _total ?? "", notiCount: _count ?? 0, cartList: cartArr)
                    let notiCount = String(_count ?? 0)
                    KeychainWrapper.standard.set(notiCount, forKey: "notiCount")
                    AppInstance.hideLoader()
                    completion(true, "", listSCM)
                } else {
                    AppInstance.hideLoader()
                    completion(true, "Items not found!", listSCM)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! try after some time", listSCM)
                
            }
            
            
        }
        
        
        
    }
    
}
