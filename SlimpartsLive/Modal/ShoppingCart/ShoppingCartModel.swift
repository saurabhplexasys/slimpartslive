//
//  ShoppingCartModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 13/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct ShoppingCartModel {
    let tax: String?
    let subtotal: String?
    let shipping: String?
    let total: String?
    let notiCount: Int?
    let cartList: [CartListSCM]
}

struct CartListSCM {
    let id: Int?
    let totQuan: Int?
    let name: String?
    let price: String?
    let totalPrice: String?
    let count: Int?
    let image: String?
    let vehicleName: String?
    let marginPrice: Double?
    let code: String?
    let supcode: String?
    let btw: String?
    let extraFee: Double?
}
