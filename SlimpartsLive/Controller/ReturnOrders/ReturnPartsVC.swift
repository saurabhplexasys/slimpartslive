//
//  ReturnPartsVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 03/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class ReturnPartsVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    

    @IBOutlet weak var appTypeTF: UITextField!
    @IBOutlet weak var orderNoTF: UITextField!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var yourQueTV: UITextView!
    @IBOutlet weak var choseBtn: UIButton!
    
    @IBOutlet weak var noFileChoseLbl: UILabel!
    
    var orderData: OrderHistoryModel?
    
    var apiHit = ReturnPartsViewModel()
    
    var userId = ""
    var orderId = ""
    var imageBase64 = ""
    var sampleImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.navigationItem.title = "Returning parts"
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
        }
        if let id = orderData?.id {
            orderId = String(id)
        }
        if let no = orderData?.orderNo {
            orderNoTF.text = no
        }
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func choseBtnAction(_ sender: UIButton) {
        print("Choose btn clicked")
        self.openCameraPhotoLibrary()
    }
    
    //Mark: Open Camera and Photolibrary
    func openCameraPhotoLibrary() {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        var actionSheet = UIAlertController()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            actionSheet = UIAlertController(title: "Photo Source", message: "", preferredStyle: .alert)
        } else {
            actionSheet = UIAlertController(title: "Photo Source", message: "", preferredStyle: .actionSheet)
        }
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let getImage = info[.editedImage] as? UIImage {
            self.sampleImage = getImage
            let editImage = sampleImage?.resized(withPercentage: 0.5)
            //self.profileImg.image = editImage
            let imageData = editImage!.pngData()
            self.imageBase64 = (imageData?.base64EncodedString())!
            if self.imageBase64.count > 1 {
                self.noFileChoseLbl.text = "Image Selected"
            } else {
                self.noFileChoseLbl.text = "No file chosen"
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendBtnAction(_ sender: UIButton) {
        print("Send btn clicked")
        if appTypeTF.text!.count > 0 {
            if titleTF.text!.count > 0 {
                if yourQueTV.text!.count > 0 {
                    //send api hit
                    self.sendApiCall()
                } else {
                    self.showAlert(title: "Please fill the 'your question' field", message: "")
                }
            } else {
                self.showAlert(title: "Please fill the 'title' field", message: "")
            }
        } else {
            self.showAlert(title: "Please fill the 'type of application' field", message: "")
        }
        
    }
    
    func sendApiCall() {
        apiHit.returnPartsOtherReasonAPiCall(orderId: orderId, type: self.appTypeTF.text!, title: self.titleTF.text!, question: self.yourQueTV.text!, image: imageBase64, userId: userId) { (_isTrue, _message) in
            if _isTrue {
                self.moveBackAfterSuccess(_message: _message)
            } else {
                self.showAlert(title: _message, message: "")
            }
         }
    }
    
    func moveBackAfterSuccess(_message: String) {
        let alert = UIAlertController(title: "Success", message: _message, preferredStyle: .alert)
        let okAct = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.goToOrdersScreen()
        }
        alert.addAction(okAct)
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToOrdersScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyOrdersVC") as! MyOrdersVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyOrdersVC") as! MyOrdersVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func closeBtnAction(_ sender: UIButton) {
        print("Close btn clicked")
        self.navigationController?.popViewController(animated: true)
    }

}

