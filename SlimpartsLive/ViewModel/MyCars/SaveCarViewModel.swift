//
//  SaveCarViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 01/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class SaveCarViewModel {
    
    func saveCarsApiCall(kType: String, make: String, model: String, engineType: String, userId: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        let url = Slimparts.baseURL + Slimparts.saveUserCarsURL
        let parameter = ["ktyps": kType, "brand": make, "modal": model, "type": engineType, "user_id": userId]
        
        AF.request(url, method: .post, parameters: parameter).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["ResponseCode"] as? String, let message = responseObj["msg"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! pleae try after some time")
                    return
                }
                if status == "1" {
                    AppInstance.hideLoader()
                    completion(true, message)
                } else {
                    AppInstance.hideLoader()
                    completion(false, message)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time")
            }
        }
        
    }
    
}
