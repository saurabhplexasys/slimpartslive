//
//  MyCarsSubCategoryVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 21/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class MyCarsSubCategoryVC: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var catTitleLbl: UILabel!
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    
    var carList: CarListModel?
    var kTypeCode = ""
    var carNameTitle = ""
    
    var catData: MenuItemCatModel?
    
    var subData = [MenuPartCatModel]()
    var subSearchData = [MenuPartCatModel]()
    
    var vaiCar = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        self.setMenuBarBtn()
        self.navigationController?.navigationBar.tintColor = .white
        self.catTitleLbl.text = catData?.menu
        
        self.subData = (self.catData?.menuPart)!
        self.subSearchData = self.subData
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            subSearchData = subData
            self.tableView.reloadData()
            return
        }
        subSearchData = subData.filter({ (SubTitle) -> Bool in
            (SubTitle.desc!.lowercased().contains(searchText.lowercased()))
        })
        
        self.tableView.reloadData()
    }

}

extension MyCarsSubCategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subSearchData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCarSubCatTVCell", for: indexPath) as! MyCarSubCatTVCell
        
        cell.titleLbl.text = subSearchData[indexPath.row].desc
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyCarsProductVC") as! MyCarsProductVC
            vc.carNameTitle = self.carNameTitle
            vc.menuCode = catData?.menuCode ?? ""
            vc.partCode = subSearchData[indexPath.row].partCode ?? ""
            vc.kTypeCode = self.kTypeCode
            vc.vaiCar = self.vaiCar
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyCarsProductVC") as! MyCarsProductVC
            vc.carNameTitle = self.carNameTitle
            vc.menuCode = catData?.menuCode ?? ""
            vc.partCode = subSearchData[indexPath.row].partCode ?? ""
            vc.kTypeCode = self.kTypeCode
            vc.vaiCar = self.vaiCar
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
