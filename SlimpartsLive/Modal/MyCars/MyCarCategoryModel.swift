//
//  MyCarCategoryModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 21/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct MyCarCategoryModel {
    let menuItem: [MenuItemCatModel]?
}

struct MenuItemCatModel {
    let menuCode: String?
    let menu: String?
    let state: String?
    let menuPart: [MenuPartCatModel]?
}

struct MenuPartCatModel {
    let desc: String?
    let partCode: String?
    let sup: [SupCatModel]?
}

struct SupCatModel {
    let desc: String?
    let supCode: String?
    let menuId: String?
}
