//
//  MyCarCategoryViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 21/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class MyCarCategoryViewModel {
    
    func getCarCatSubCatAPiCall(completion: @escaping(Bool, String, MyCarCategoryModel) -> Void) {
        
        AppInstance.showLoader()
        let url = Slimparts.baseURL + Slimparts.carCategoryURL
        
        var menuPartData = [MenuPartCatModel]()
        menuPartData.removeAll()
        
        var supData = [SupCatModel]()
        supData.removeAll()
        
        var menuItemData = [MenuItemCatModel]()
        menuItemData.removeAll()
        
        var catData = MyCarCategoryModel(menuItem: menuItemData)
        
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["ResponseCode"] as? String, let message = responseObj["ResponseText"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time", catData)
                    return
                }
                if status == "1" {
                    
                    let _data = responseObj["data"] as? [String: Any]
                    let _menuItem = _data!["Menuitem"] as? [[String: Any]]
                    
                    for items in _menuItem! {
                        let _menuCode = items["menucode"] as? String
                        let _menu = items["menu"] as? String
                        let _state = items["state"] as? String
                        
                        if _state == "0" {
                                
                            let _menuPart = items["Menupart"] as? [[String: Any]]
                            if _menuPart != nil {
                                for parts in _menuPart! {
                                     let _descr = parts["descr"] as? String
                                     let _partCode = parts["partcode"] as? String
                                     let _supArr = parts["sup"] as? [[String: Any]]
                                     
                                     if _supArr != nil {
                                         for sup in _supArr! {
                                             let _desc = sup["descr"] as? String
                                             let _supCode = sup["supcode"] as? String
                                             let _menuId = sup["menuid"] as? String
                                             
                                             let _supData = SupCatModel(desc: _desc ?? "", supCode: _supCode ?? "", menuId: _menuId ?? "")
                                             supData.append(_supData)
                                             
                                         }
                                     } else {
                                         let _sup = parts["sup"] as? [String: Any]
                                         let _desc = _sup!["descr"] as? String
                                         let _supCode = _sup!["supcode"] as? String
                                         let _menuId = _sup!["menuid"] as? String
                                         
                                         let _supData = SupCatModel(desc: _desc ?? "", supCode: _supCode ?? "", menuId: _menuId ?? "")
                                         supData.append(_supData)
                                     }
                                     
                                     let _menuPartData = MenuPartCatModel(desc: _descr ?? "", partCode: _partCode ?? "", sup: supData)
                                     menuPartData.append(_menuPartData)
                                 }
//                                let _menuItemData = MenuItemCatModel(menuCode: _menuCode ?? "", menu: _menu ?? "", state: _state ?? "", menuPart: menuPartData)
//                                menuItemData.append(_menuItemData)
                                
                            } else {
                                
                                let _menuPart = items["Menupart"] as? [String: Any]
                                let _descr = _menuPart!["descr"] as? String
                                let _partCode = _menuPart!["partcode"] as? String
                                let _supArr = _menuPart!["sup"] as? [[String: Any]]
                                if _supArr != nil {
                                    for sup in _supArr! {
                                        let _desc = sup["descr"] as? String
                                        let _supCode = sup["supcode"] as? String
                                        let _menuId = sup["menuid"] as? String
                                        
                                        let _supData = SupCatModel(desc: _desc ?? "", supCode: _supCode ?? "", menuId: _menuId ?? "")
                                        supData.append(_supData)
                                        
                                    }
                                } else {
                                    let _sup = _menuPart!["sup"] as? [String: Any]
                                    let _desc = _sup!["descr"] as? String
                                    let _supCode = _sup!["supcode"] as? String
                                    let _menuId = _sup!["menuid"] as? String
                                    
                                    let _supData = SupCatModel(desc: _desc ?? "", supCode: _supCode ?? "", menuId: _menuId ?? "")
                                    supData.append(_supData)
                                }
                                let _menuPartData = MenuPartCatModel(desc: _descr ?? "", partCode: _partCode ?? "", sup: supData)
                                menuPartData.append(_menuPartData)
                                
                            }
                            let _menuItemData = MenuItemCatModel(menuCode: _menuCode ?? "", menu: _menu ?? "", state: _state ?? "", menuPart: menuPartData)
                            menuItemData.append(_menuItemData)
                        }
                        
                    }
                    catData = MyCarCategoryModel(menuItem: menuItemData)
                    AppInstance.hideLoader()
                    completion(true, message, catData)
                } else {
                    AppInstance.hideLoader()
                    completion(false, message, catData)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time", catData)
                
            }
            
        }
        
        
    }
    
}
