//
//  CheckoutUserAddressVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 19/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class CheckoutUserAddressVC: UIViewController {

    @IBOutlet weak var emailTFCD: UITextField!
    @IBOutlet weak var firstNameTFCD: UITextField!
    @IBOutlet weak var lastNameTFCD: UITextField!
    @IBOutlet weak var mobileNoTFCD: UITextField!
    
    @IBOutlet weak var countryTFBD: UITextField!
    @IBOutlet weak var houseNoTFBD: UITextField!
    @IBOutlet weak var addressTFBD: UITextField!
    @IBOutlet weak var cityTFBD: UITextField!
    @IBOutlet weak var psotCodeTFBD: UITextField!
    
    @IBOutlet weak var countryTFSD: UITextField!
    @IBOutlet weak var houseNoTFSD: UITextField!
    @IBOutlet weak var addressTFSD: UITextField!
    @IBOutlet weak var cityTFSD: UITextField!
    @IBOutlet weak var psotCodeTFSD: UITextField!
    
    @IBOutlet weak var sameAsBillBtnOutlet: UIButton!
    @IBOutlet weak var countryBDBtnOutlet: UIButton!
    @IBOutlet weak var countrySDBtnOutlet: UIButton!
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    
    var userId = ""
    var userProfile = ProfileViewModel()
    var userProfileData : ProfileModel?
    
    var getDetails = UserDetailsViewModel()
    var userDetails: UserDetailsModel?
    
    var userUpdateAdd = ProfileAddressUpdateViewModel()
    
    var cartList: ShoppingCartModel?
    
    var billId = 0
    var shiId = 0
    
    var iconClick = true {
        didSet {
            if iconClick {
                let image = UIImage(named: "rightTick")
                sameAsBillBtnOutlet.setImage(image, for: .normal)
                self.addressTFSD.text = self.addressTFBD.text
                self.cityTFSD.text = self.cityTFBD.text
                self.psotCodeTFSD.text = self.psotCodeTFBD.text
                self.houseNoTFSD.text = self.houseNoTFBD.text
            } else {
                let image = UIImage(named: "")
                sameAsBillBtnOutlet.setImage(image, for: .normal)
                self.addressTFSD.text = ""
                self.cityTFSD.text = ""
                self.psotCodeTFSD.text = ""
                self.houseNoTFSD.text = ""
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.navigationController?.navigationBar.tintColor = .white
        self.setMenuBarBtn()
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
            print("user id:- ", self.userId)
            self.updateProfilePersonalDetails()
            self.getUserDetailsAPiCall()
        }
        
        // Do any additional setup after loading the view.
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func getUserDetailsAPiCall() {
        getDetails.getUserDatailsAPI(userId: userId) { (_isTrue, _message, _userData) in
            if _isTrue {
                self.userDetails = _userData
                DispatchQueue.main.async {
                    self.updateProfileAfterGetUserDetialsAPI()
                    self.updateAddressAfterGetUserDetailsAPI()
                }
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    func updateProfileAfterGetUserDetialsAPI() {
        let userName = (userDetails?.firstName ?? "") + " " + (userDetails?.lastName ?? "")
        KeychainWrapper.standard.set(userName, forKey: "userName")
        KeychainWrapper.standard.set(userDetails?.email ?? "", forKey: "email")
        KeychainWrapper.standard.set(userDetails?.mobile ?? "", forKey: "mobile")
        self.updateProfilePersonalDetails()
    }
    
    func updateAddressAfterGetUserDetailsAPI() {
        if !(userDetails?.addressDetail!.isEmpty)! {
            for detail in (userDetails?.addressDetail)! {
                let addType = detail.addressType
                if addType == "billing" {
                    self.addressTFBD.text = detail.address
                    self.cityTFBD.text = detail.city
                    self.psotCodeTFBD.text = detail.postCode
                    self.houseNoTFBD.text = detail.houseNo
                    self.billId = detail.addressId ?? 0
                } else {
                    self.addressTFSD.text = detail.address
                    self.cityTFSD.text = detail.city
                    self.psotCodeTFSD.text = detail.postCode
                    self.houseNoTFSD.text = detail.houseNo
                    self.shiId = detail.addressId ?? 0
                }
            }
        }
    }
    
    func updateProfilePersonalDetails() {
        
        if let userName = KeychainWrapper.standard.string(forKey: "userName") {
            let fullName    = userName
            let fullNameArr = fullName.components(separatedBy: " ")

            let name    = fullNameArr[0]
            let surname = fullNameArr[1]
            self.firstNameTFCD.text = name
            self.lastNameTFCD.text = surname
        }
        if let email = KeychainWrapper.standard.string(forKey: "email") {
            self.emailTFCD.text = email
        }
        if let mobile = KeychainWrapper.standard.string(forKey: "mobile") {
            self.mobileNoTFCD.text = mobile
        }
        
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryBillingBtnAction(sender: UIButton) {
        print("country billing btn pressed")
    }
    
    @IBAction func countryShippingBtnAction(sender: UIButton) {
        print("country shipping btn pressed")
    }
    
    @IBAction func sameAsbillBtnAction(sender: UIButton) {
        print("same as billing btn pressed")
        iconClick = !iconClick
        UIView.animate(withDuration: 0, delay: 0, options: .transitionCrossDissolve, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) { (success) in
            sender.isSelected = !sender.isSelected
            UIView.animate(withDuration: 0, delay: 0, options: .transitionCrossDissolve, animations: {
                sender.transform = .identity
            }, completion: nil)
        }
    }
    
    
    @IBAction func placeOrderBtnAction(sender: UIButton) {
        print("update address btn pressed")
        self.addressValidation()
    }
    
    func addressValidation() {
        if addressTFBD.text!.count > 0 {
            if cityTFBD.text!.count > 0 {
                if psotCodeTFBD.text!.count > 0 {
                        if addressTFSD.text!.count > 0 {
                            if cityTFSD.text!.count > 0 {
                                if psotCodeTFSD.text!.count > 0 {
                                    
                                    //Go for place order
                                    print("you can go for place order")
                                    
                                    var addDetails = [AddressDetails]()
                                    addDetails.removeAll()
                                    
                                    let addBill = AddressDetails(address: addressTFBD.text!, city: cityTFBD.text!, postCode: psotCodeTFBD.text!, addressId: billId, addressType: "billing", houseNo: houseNoTFBD.text ?? "")
                                    
                                    addDetails.append(addBill)
                                    
                                    let addShi = AddressDetails(address: addressTFSD.text!, city: cityTFSD.text!, postCode: psotCodeTFSD.text!, addressId: shiId, addressType: "shipping", houseNo: houseNoTFSD.text ?? "")
                                    
                                    addDetails.append(addShi)
                                    
                                    userDetails = UserDetailsModel(firstName: firstNameTFCD.text ?? "", lastName: lastNameTFCD.text ?? "", email: emailTFCD.text ?? "", mobile: mobileNoTFCD.text ?? "", addressDetail: addDetails)
                                    
                                    self.moveToNextVC()
                                    
                                } else {
                                    self.showAlert(title: "Alert", message: "please enter shipping postcode")
                                }
                            } else {
                                self.showAlert(title: "Alert", message: "please enter shipping town/city")
                            }
                        } else {
                            self.showAlert(title: "Alert", message: "please enter shipping address")
                        }
                    
                } else {
                    self.showAlert(title: "Alert", message: "please enter billing postcode")
                }
            } else {
                self.showAlert(title: "Alert", message: "please enter billing town/city")
            }
        } else {
            self.showAlert(title: "Alert", message: "please enter billing address")
        }
    }
    
    func moveToNextVC() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CheckoutOrderVC") as! CheckoutOrderVC
            vc.cartList = self.cartList
            vc.userDetails = self.userDetails
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckoutOrderVC") as! CheckoutOrderVC
            vc.cartList = self.cartList
            vc.userDetails = self.userDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
