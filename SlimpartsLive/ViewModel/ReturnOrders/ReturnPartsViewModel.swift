//
//  ReturnPartsViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 04/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class ReturnPartsViewModel {
    
    func returnPartsOtherReasonAPiCall(orderId: String, type: String, title: String, question: String, image: String, userId: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        
        let header: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let url = Slimparts.baseURL + Slimparts.returnOrderOtherReasonURL
        
        let parameter = [ "orderId": orderId,
           "type" : type,
           "title"  : title,
           "question" : question,
           "image" : image,
           "user_id": userId]
        
        AF.request(url, method: .post, parameters: parameter, headers: header).responseJSON { (response) in
            
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any] else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time")
                    return
                }
                let status = responseObj["status"] as? String
                if status == "success" {
                    AppInstance.hideLoader()
                    completion(true, "Your Return Order Request registered Successfully.")
                } else {
                    AppInstance.hideLoader()
                    completion(false, "Sent Failed, please try after some time")
                }
                
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time")
            }
            
        }
        
        
    }
    
}
