//
//  OrderHistoryModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 29/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct OrderHistoryModel {
    let date: String?
    let id: Int?
    let orderNo: String?
    let total: String?
    let status: String?
    let payStatus: String?
    let deliveryDate: String?
    let invoiceNo: Int?
    let invoicePdf: String?
}
