//
//  DeleteCartItemViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 19/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

class DeleteCartItemViewModel {
    
    func deleteCartItemApiCall(userId: String, cartId: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        let url = Slimparts.baseURL + Slimparts.deleteCartItemURL
        let parameter = ["user_id": userId, "cart_id": cartId]
        
        AF.request(url, method: .post, parameters: parameter).responseJSON { (response) in
            
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String:Any], let message = responseObj["msg"] as? String, let status = responseObj["status"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! try after some time")
                    return
                }
                if status == "success" {
                    let count = responseObj["count"] as? Int
                    let notiCount = String(count ?? 0)
                    KeychainWrapper.standard.set(notiCount, forKey: "notiCount")
                    AppInstance.hideLoader()
                    completion(true, message)
                } else {
                    AppInstance.hideLoader()
                    completion(false, message)
                }
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! try after some time")
            }
        }
        
    }
    
}
