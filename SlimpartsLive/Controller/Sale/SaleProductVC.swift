//
//  SaleProductVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 05/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftKeychainWrapper

class SaleProductVC: UIViewController {
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var iIconBtn: UIButton!
    
    let cartButton = SSBadgeButton()
    
    var catData: SaleModel?
    var subCatData: SubCategoryAccessories?
    
    var saleProList = [SaleProductModel]()
    var saleUser = SaleProductViewModel()
    var userId = ""
    var cartItem: [[String: AnyObject]] = []
    var isItemAdded = false
    
    var userAddCart = AddToCartSaleProductViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.setMenuBarBtn()
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            userId = userid
        }
        self.titleSetup()
        self.saleProListAPiCall()
        // Do any additional setup after loading the view.
    }
    
    func saleProListAPiCall() {
        saleUser.getSaleProductList(catId: (catData?.catId)!, subCatId: (subCatData?.subCatId)!){ (_isTrue, _message, _proData) in
            if _isTrue {
                self.saleProList = _proData
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                let alert = UIAlertController(title: _message, message: "", preferredStyle: .alert)
                let okAct = UIAlertAction(title: "Ok", style: .default) { (action) in
                    self.navigationController?.popViewController(animated: true)
                }
                alert.addAction(okAct)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func titleSetup() {
        let menuName = "Sale"
        let catName = ">" + (catData?.catName ?? "")
        let subCatName = ">" + (subCatData?.subCatName ?? "")
        self.titleLbl.text = menuName + catName + subCatName
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cartBtnView()
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func cartBtnView() {
        cartButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        cartButton.setImage(UIImage(named: "cartIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        cartButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        cartButton.tintColor = .white
        
        cartButton.addTarget(self, action: #selector(cartButtonTapped), for: .touchUpInside)
        
        self.updateCartCount()
        
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: cartButton)]
    }
    
    func updateCartCount() {
        if let count = KeychainWrapper.standard.string(forKey: "notiCount") {
            cartButton.badgeBackgroundColor = .red
            cartButton.badge = count
        } else {
            cartButton.badgeBackgroundColor = .clear
            cartButton.badge = ""
        }
    }
    
    @objc func cartButtonTapped() {
        print("cart button pressed")
        self.moveToCartScreen()
    }
    
    func moveToCartScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func iIconBtnAction(_ sender: UIButton) {
        
        
    }
    
    
}

extension SaleProductVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.saleProList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SaleProductTVCell", for: indexPath) as! SaleProductTVCell
        
        cell.proImg.sd_setImage(with: URL(string: saleProList[indexPath.row].proImg ?? "defaultImage")) { (image, error, cache, url) in
            if error != nil {
                cell.proImg.image = UIImage(named: "defaultImage")
            } else {
                cell.proImg.image = image
            }
        }
        
        cell.proNameLbl.text = saleProList[indexPath.row].proName
        cell.proCodeLbl.text = "SKU: " + (saleProList[indexPath.row].proUniqId ?? "Not Found")
        cell.proPriceLbl.text = "Price: " + (saleProList[indexPath.row].proPrice ?? "Not Found")
        cell.viewDetailBtnOutlet.tag = indexPath.row
        cell.viewDetailBtnOutlet.addTarget(self, action: #selector(viewDetialsAction(_:)), for: .touchUpInside)
        
        cell.quantityView.isHidden = true
        
        /* //quantity increase and decrease
        if let quantity = saleProList[indexPath.row].proQuantity {
            if quantity > 0 {
                cell.quantityView.isHidden = false
                cell.minusBtnOutlet.tag = indexPath.row
                cell.plusBtnOutlet.tag = indexPath.row
                cell.minusBtnOutlet.addTarget(self, action: #selector(minusBtnAction(_:)), for: .touchUpInside)
                cell.plusBtnOutlet.addTarget(self, action: #selector(plusBtnAction(_:)), for: .touchUpInside)
            } else {
                cell.quantityView.isHidden = true
            }
        } else {
            cell.quantityView.isHidden = true
        } */
        
        
        cell.addToCartView.isHidden = false
        //add to cart functionality
        if let quantity = saleProList[indexPath.row].proQuantity {
            if quantity > 0 {
                cell.addToCartBtn.setTitle("Add to Cart", for: .normal)
                cell.addToCartBtn.setTitleColor(UIColor.init(displayP3Red: 00/255, green: 122/255, blue: 255/255, alpha: 1), for: .normal)
                cell.addToCartBtn.isEnabled = true
                cell.addToCartBtn.tag = indexPath.row
                cell.addToCartBtn.addTarget(self, action: #selector(addToCartAction(_:)), for: .touchUpInside)
                
            } else {
                cell.addToCartBtn.setTitle("Out Of Stock", for: .normal)
                cell.addToCartBtn.setTitleColor(.orange, for: .normal)
                cell.addToCartBtn.isEnabled = false
            }
        } else {
            cell.addToCartBtn.setTitle("Out Of Stock", for: .normal)
            cell.addToCartBtn.isEnabled = false
        }
        
        return cell
    }
    
    @objc func addToCartAction(_ sender: UIButton) {
        
        if userId == "" {
            //if not get userId then add product to locally
            
            var productImage = ""
            if let proImage = saleProList[sender.tag].proImg {
                productImage = proImage
            }
            let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! SaleProductTVCell
            let quantity = cell.quantityTF.text
            
            if let loadedCart = UserDefaults().array(forKey: "myCart") as? [[String: AnyObject]] {
                for item in loadedCart {
                    if saleProList[sender.tag].proId == item["proId"] as? Int {
                        isItemAdded = true
                    }
                }
            }
            
            if isItemAdded == true {
                isItemAdded = false
                self.showAlert(title: "item already added", message: "")
            } else {
                isItemAdded = false
                cartItem.append(["proId": saleProList[sender.tag].proId as AnyObject, "proUniqId": saleProList[sender.tag].proUniqId as AnyObject, "proName": saleProList[sender.tag].proName as AnyObject, "proPrice": saleProList[sender.tag].proPrice as AnyObject, "proDesc": saleProList[sender.tag].proDesc as AnyObject, "proImg": productImage as AnyObject, "proQuantity": quantity as AnyObject, "status": saleProList[sender.tag].status as AnyObject, "catId": saleProList[sender.tag].catId as AnyObject, "subCatId": saleProList[sender.tag].subCatId as AnyObject, "brand": saleProList[sender.tag].brand as AnyObject])
                
                UserDefaults.standard.set(cartItem, forKey: "cartItem")
                
                if let saveItem = UserDefaults.standard.array(forKey: "cartItem") {
                    print("Save item are :- ", saveItem)
                }
                
                let count: String = String(cartItem.count)
                
                KeychainWrapper.standard.set(count, forKey: "notiCount")
                
                self.showAlert(title: "item save successfully", message: "")
                
            }
            
            //To fetch above cartItem from user default
            //                if let loadedCart = NSUserDefaults().arrayForKey("myCart") as? [[String: AnyObject]] {
            //                    print(loadedCart)  // [[price: 19.99, qty: 1, name: A], [price: 4.99, qty: 2, name: B]]"
            //
            //                    for item in loadedCart {
            //                        print(item["name"] as! String)  // A, B
            //                        print(item["price"] as! Double) // 19.99, 4.99
            //                        print(item["qty"] as! Int)      // 1, 2
            //                    }
            //                }
            
            
        } else {
            //hit add to cart api: only login user can add to cart directly to server
            print("*****Hit add to cart api*****")
            self.addToCartAPICAll(name: saleProList[sender.tag].proName ?? "", price: saleProList[sender.tag].proPrice ?? "0.0", quantity: "1", vehicleName: saleProList[sender.tag].proName ?? "", image: saleProList[sender.tag].proImg ?? "", uniCode: saleProList[sender.tag].proUniqId ?? "", totalQauntity: saleProList[sender.tag].proQuantity ?? 0, proId: saleProList[sender.tag].proId ?? 0)
            
            
        }
        
    }
    
    func addToCartAPICAll(name: String, price: String, quantity: String, vehicleName: String, image: String, uniCode: String, totalQauntity: Int, proId: Int) {
        
        let price = price
        let priceDouble = Double(price)!
        let quan = quantity
        let quanInt = Int(quan)!
        
        let proIdStr = String(proId)
        
        userAddCart.addToCartSaleProductApiCall(name: name, price: priceDouble, marginPrice: 0, quantity: quanInt, vehicleName: name, image: image, type: 0, uniCode: uniCode, totalQuantity: totalQauntity, supCode: "0", userId: Int(self.userId)!, productId: proIdStr, itemNo: "", itemGroup: 0, btw: "", extraFee: "") { (_isTrue, _message) in
            if _isTrue {
                self.updateCartCount()
                self.showAlert(title: _message, message: "")
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
        
    }
    
    @objc func minusBtnAction(_ sender: UIButton) {
        
        print("minus btn press")
        let quantity = saleProList[sender.tag].proQuantity
        let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! SaleProductTVCell
        let quaTxt = Int(cell.quantityTF.text ?? "1")
        if quaTxt == 1 {
            cell.quantityTF.text = "1"
        } else if quaTxt! >= 1 || quaTxt! <= quantity ?? 1{
            cell.quantityTF.text = String(quaTxt! - 1)
        } else {
            cell.quantityTF.text = "1"
        }
    }
    
    @objc func plusBtnAction(_ sender: UIButton) {
        
        print("plus btn press")
        let quantity = saleProList[sender.tag].proQuantity
        let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! SaleProductTVCell
        let quaTxt = Int(cell.quantityTF.text ?? "1")
        if quaTxt! <= quantity ?? 1{
            if quaTxt == quantity {
                self.showAlert(title: "You can not add more than \(quaTxt ?? 0) units", message: "")
            } else {
                let out = (quaTxt! + 1)
                cell.quantityTF.text = String(out)
            }
        } else {
            cell.quantityTF.text = "1"
        }
    }
    
    @objc func viewDetialsAction(_ sender: UIButton) {
        print("View Details Btn Pressed")
        print(saleProList[sender.tag])
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SaleProductDetailsVC") as! SaleProductDetailsVC
            vc.proDetails = saleProList[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SaleProductDetailsVC") as! SaleProductDetailsVC
            vc.proDetails = saleProList[sender.tag]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
