//
//  UserDetailsModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 01/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct UserDetailsModel {
    let firstName: String?
    let lastName: String?
    let email: String?
    let mobile: String?
    let addressDetail: [AddressDetails]?
}

struct AddressDetails {
    let address: String?
    let city: String?
    let postCode: String?
    let addressId: Int?
    let addressType: String?
    let houseNo: String?
}
