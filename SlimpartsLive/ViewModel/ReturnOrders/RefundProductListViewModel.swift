//
//  ReturnProductListViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 05/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class RefundProductListViewModel {
    
    func getRefProListApiCall(userId: String, orderId: String, completion: @escaping(Bool, String, [RefundProductListModel])-> Void) {
        
        AppInstance.showLoader()
        
        var listData = [RefundProductListModel]()
        listData.removeAll()
        
        let getExtraUrl = "\(orderId)/\(userId)"
        
        let url = Slimparts.baseURL + Slimparts.refundProductListURL + getExtraUrl
        
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any] else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time", listData)
                    return
                }
                
                let _cartList = responseObj["cartlist"] as? [[String: Any]]
                if !_cartList!.isEmpty {
                    
                    for list in _cartList! {
                        
                        let _id = list["id"] as? Int
                        let _proId = list["product_id"] as? Int
                        let _proName = list["product_name"] as? String
                        let _totPrice = list["total_price"] as? String
                        let _quantity = list["quantity"] as? Int
                        let _proImg = list["product_image"] as? String
                        let _orderFor = list["order_for"] as? String
                        let _proCode = list["product_code"] as? String
                        let _supCode = list["supcode"] as? String
                        let _orderId = list["order_id"] as? Int
                        let _userId = list["user_id"] as? Int
                        let _refund = list["refund"] as? Int
                        let _refundId = list["refund_id"] as? Int
                        
                        let data = RefundProductListModel(id: _id ?? 0, proId: _proId ?? 0, proName: _proName ?? "", totalPrice: _totPrice ?? "", quantity: _quantity ?? 0, proImg: _proImg ?? "", orderFor: _orderFor ?? "", proCode: _proCode ?? "", supCode: _supCode ?? "", orderId: _orderId ?? 0, userId: _userId ?? 0, refund: _refund ?? 0, refundId: _refundId ?? 0)
                        
                        listData.append(data)
                        
                    }
                    
                    AppInstance.hideLoader()
                    completion(true, "", listData)
                    
                } else {
                    AppInstance.hideLoader()
                    completion(false, "Data Not Found", listData)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time", listData)
            }
            
        }
        
    }
    
}
