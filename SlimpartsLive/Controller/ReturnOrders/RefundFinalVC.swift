//
//  RefundFinalVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 08/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class RefundFinalVC: UIViewController {

    let apiHit = RefundFinalViewModel()
    
    var orderId = ""
    var userId = ""
    var cartIdArr = [Int]()
    var quantityArr = [Int]()
    var reasonStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.navigationItem.title = "Refund procedure"
        
        // Do any additional setup after loading the view.
    }
    
    func refundFinalApiCall() {
        apiHit.refundFinalApiCall(orderId: orderId, cartID: cartIdArr, quntity: quantityArr, reason: reasonStr, userId: userId) { (_isTrue, _message) in
            if _isTrue {
                self.moveBackAfterSuccess(_message: _message)
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
    }
    
    func moveBackAfterSuccess(_message: String) {
        
        self.showAlertAction(title: "Success", message: _message, alertStyle: .alert, actionTitles: ["Ok"], actionStyles: [.default], actions: [{_ in
            self.goToOrdersScreen()
            }])
        
    }
    
    func goToOrdersScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyOrdersVC") as! MyOrdersVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyOrdersVC") as! MyOrdersVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        
        self.refundFinalApiCall()
        
    }
    


}
