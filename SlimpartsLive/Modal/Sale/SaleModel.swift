//
//  AccessorieModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 04/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct SaleModel {
    let catId: Int?
    let catName: String?
    let subCat: [SubCategoryAccessories]?
}

struct SubCategoryAccessories {
    let subCatId: Int?
    let subCatName: String?
}
