//
//  Constant.swift
//  Slimparts
//
//  Created by Dharmesh Kothari on 22/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import UIKit

struct Slimparts {
    static let testBaseUrl = "https://slimparts.nl/"
    static let liveBaseURL = "https://slimparts.nl/"
    static let testOneSignalId = ""
    static let liveOneSignalId = ""
    
    static var baseURL = testBaseUrl
    static var oneSignalId = testOneSignalId
    
    static var authKey = "1234567"
    
    static var navBgColor = UIColor(displayP3Red: 0/255, green: 84/255, blue: 147/255, alpha: 1)
    
    static let loginURL = "v1/login"
    static let registerURL = "v1/register"
    static let makeURL = "v1/make"
    static let makeModelURL = "v1/make-modal?makecode="
    static let getEngineTypeURL = "v1/makeModalType?modelcode="
    static let searchByktypeURL = "v1/searchByKtype?ktyps="
    static let searchByRegUrl = "v1/searchByPlate?plate="
    static let profileUpdateURL = "v1/update-profile"
    static let addUserAddressURL = "v1/add-user-address"
    static let getUserDetailsURL = "v1/user-detail/"
    static let changePasswordUrl = "v1/change-password"
    static let forgotPasswordUrl = "v1/forgotpass"
    static let saveUserCarsURL = "v1/save-user-cars"
    static let userCarListURL = "v1/user-car-listing/"
    static let deleteCarFromListURL = "v1/delete-user-car/"
    static let getCategoryAndSubcatURL = "v1/filter-category-listing"
    static let getSaleProductListURL = "v1/products/"
    static let getCarDetailsWithTypeCodeURL = "v1/car-detail?typecodes="
    static let addToCartSaleProductURL = "v1/add-to-cart"
    static let cartListURL = "v1/cart/"
    static let updateQuantityCartURL = "v1/update-cart"
    static let deleteCartItemURL = "v1/cart"
    static let carCategoryURL = "v1/categoery?type=1&code=0"
    
    static let carSearchByKtypeURL = "v1/searchByKtype?ktyps="
    static let orderHistoryURL = "v1/order-history/"
    static let viewOrderHistoryURL = "v1/order-details/"
    static let returnOrderOtherReasonURL = "v1/order-refund-other"
    static let refundProductListURL = "v1/cart-details/"
    static let refundFinalURL = "v1/order-refund-request"
    
    static let paymentURL = "v1/payment"
    
    //Keychain wrapper constant
    static let userID = "userID"
    static let userName = "userName"
    static let email = "email"
    static let mobile = "mobile"
    static let notiCount = "notiCount"
    static let typeCode = "typeCode"
    static let menuCode = "menuCode"
    static let makeName = "makeName"
    static let modelName = "modelName"
    static let engineName = "engineName"
    
    
    
}
