//
//  EngineTypeViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 31/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class EngineTypeViewModel {
    
    func getEngineTypeData(modelCode: String, completion : @escaping(Bool, String, [EngineTypeModel]) -> Void) {
        
        AppInstance.showLoader()
        
        
        var engineData = [EngineTypeModel]()
        engineData.removeAll()
        
        let url = Slimparts.baseURL + Slimparts.getEngineTypeURL + modelCode
                
        AF.request(url, method: .get).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["ResponseCode"] as? String, let message = responseObj["ResponseText"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem", engineData)
                    return
                }
                if status == "1" {
                    let _data = responseObj["data"] as? [[String: Any]]
                    if !_data!.isEmpty {
                        for data in _data! {
                            let _kTypeCode = data["ktyp"] as? String
                            let _typeCode = data["typecode"] as? String
                            let _typeName = data["typename"] as? String
                            
                            let md = EngineTypeModel(typeCode: _typeCode ?? "", typeName: _typeName ?? "", kTypeCode: _kTypeCode ?? "")
                            engineData.append(md)
                        }
                        AppInstance.hideLoader()
                        completion(true, message, engineData)
                    } else {
                        AppInstance.hideLoader()
                        completion(false, message, engineData)
                    }
                } else {
                    AppInstance.hideLoader()
                    completion(false, message, engineData)
                }
            case .failure(let error): 
                AppInstance.hideLoader()
                completion(false, "serverProblem", engineData)
            }
        }
        
    }
    
    
}
