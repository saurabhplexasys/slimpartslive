//
//  MyOrderViewVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 01/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class MyOrderViewVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewAdd: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderNoTitleLbl: UILabel!
    @IBOutlet weak var orderDateStatusLbl: UILabel!
    
    @IBOutlet weak var subtotalLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    
    var userId = ""
    var orderId = ""
    
    var apiHit = ViewOrderHistoryViewModel()
    var viewData: ViewOrderModel?
    
    var cartData = [CartListViewOrderModel]()
    var addressData = [AddressViewOrderModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
            print("user id:- ", self.userId)
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        
        tableViewAdd.delegate = self
        tableViewAdd.dataSource = self
        tableViewAdd.estimatedRowHeight = 170
        tableViewAdd.rowHeight = UITableView.automaticDimension
        
        self.viewOrderApiCall()
        // Do any additional setup after loading the view.
    }
    
    func viewOrderApiCall() {
        apiHit.viewOrderHistoryApiCall(orderId: orderId, userId: userId) { (_isTrue, _message, _data) in
            if _isTrue {
                print("Success View Order History")
                self.viewData = _data
                DispatchQueue.main.async {
                    self.updateViewDetails()
                }
            } else {
                self.moveBack(message: _message)
            }
        }
    }
    
    func moveBack(message: String) {
        let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateViewDetails() {
        
        self.orderNoTitleLbl.text = "Order #\(viewData?.orders?.orderNo ?? "")"
        let date = viewData?.orders?.date
        let status = viewData?.orders?.status
        let dateStatus = "Was placed on \(date ?? "") and is currently \(status ?? "")."
        self.orderDateStatusLbl.text = dateStatus
        //*******************************//
        if let list = viewData?.cartList {
            self.cartData = list
            if self.cartData.count > 1 {
                self.tableViewHeightConstraint.constant = CGFloat(44 * self.cartData.count)
            } else {
                self.tableViewHeightConstraint.constant = 44
            }
            
            self.tableView.reloadData()
            
        }
        //*******************************//
        if let subtotal = viewData?.orders?.subTotal {
            self.subtotalLbl.text = subtotal
        }
        if let shipping = viewData?.orders?.shipping {
            self.shippingLbl.text = shipping
        }
        if let tax = viewData?.orders?.tax {
            self.taxLbl.text = tax
        }
        if let total = viewData?.orders?.totalPrice {
            self.totalLbl.text = total
        }
        //*******************************//
        if let address = viewData?.address {
            self.addressData = address
            self.tableViewAdd.reloadData()
        }
        
    }
    

}

extension MyOrderViewVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return cartData.count
        } else {
            return addressData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderViewTVCell", for: indexPath) as! MyOrderViewTVCell
            
            if let name = cartData[indexPath.row].productNAme {
                if let quantity = cartData[indexPath.row].quantity {
                    let data = "\(name) x \(quantity)"
                    cell.proNameQuantityLbl.text = data
                }
            }
            if let price = cartData[indexPath.row].price {
                cell.priceLbl.text = price
            }
            
            return cell
        }
        if tableView == self.tableViewAdd {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderViewAddressTVCell", for: indexPath) as! MyOrderViewAddressTVCell
            
            var firstName = ""
            var lastName = ""
            var fullName = ""
            if let firstN = viewData?.user?.firstName {
                firstName = firstN
            }
            if let lastN = viewData?.user?.lastName {
                lastName = lastN
            }
            fullName = "\(firstName) \(lastName)"
            cell.nameLbl.text = fullName
            
            var address = ""
            var houseNo = ""
            var postCode = ""
            var city = ""
            var fullAddress = ""
            
            if let addType = addressData[indexPath.row].addType {
                if addType == "billing" {
                    cell.addressTypeLbl.text = "BILLING ADDRESS"
                } else {
                    cell.addressTypeLbl.text = "SHIPPING ADDRESS"
                }
            }
            if let adr = addressData[indexPath.row].address {
                address = adr
            }
            if let hn = addressData[indexPath.row].houseNo {
                houseNo = hn
            }
            if let pc = addressData[indexPath.row].postCode {
                postCode = pc
            }
            if let c = addressData[indexPath.row].city {
                city = c
            }
            fullAddress = "\(address) \(houseNo), \(postCode) \(city)"
            cell.addressLbl.text = fullAddress
            
            if let phone = viewData?.user?.mobile {
                cell.mobileLbl.text = phone
            }
            if let email = viewData?.user?.email {
                cell.emailLbl.text = email
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
}


