//
//  RefundProductsVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 05/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class RefundProductsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkBtnOutlet: UIButton!
    @IBOutlet weak var nextBtnOutlet: UIButton!
    
    var iconClick = false {
        didSet {
            if iconClick {
                let image = UIImage(named: "rightTick")
                checkBtnOutlet.setImage(image, for: .normal)
                
            } else {
                let image = UIImage(named: "")
                checkBtnOutlet.setImage(image, for: .normal)
                
            }
        }
    }
    
    let apiHit = RefundProductListViewModel()
    var orderData: OrderHistoryModel?
    var userId = ""
    var selectedReason = ""
    var listData = [RefundProductListModel]()
    var selectedIndex = [Int]()
    var cartIdArr = [Int]()
    var quantityArr = [Int]()
    
    var quantityTFArr = [Int]()
    var quantitySelectedArr = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationItem.title = "Refund"
        selectedIndex.removeAll()
        cartIdArr.removeAll()
        quantityArr.removeAll()
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
        }
        self.refundProductListApiCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.quantitySelectedArr.removeAll()
    }
    
    func refundProductListApiCall() {
        let id = orderData?.id ?? 0
        apiHit.getRefProListApiCall(userId: userId, orderId: String(id)) { (_isTrue, _message, _data) in
            if _isTrue {
                //success
                self.listData = _data
                for data in _data {
                    self.quantityTFArr.append(data.quantity ?? 0)
                }
                                
                self.tableView.reloadData()
            } else {
                self.moveBackAfterSuccess(_message: _message)
            }
        }
    }
    
    func moveBackAfterSuccess(_message: String) {
        let alert = UIAlertController(title: "Failed", message: _message, preferredStyle: .alert)
        let okAct = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(okAct)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func checkBtnAction(_ sender: UIButton) {
        iconClick = !iconClick
        UIView.animate(withDuration: 0, delay: 0, options: .transitionCrossDissolve, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) { (success) in
            sender.isSelected = !sender.isSelected
            UIView.animate(withDuration: 0, delay: 0, options: .transitionCrossDissolve, animations: {
                sender.transform = .identity
            }, completion: nil)
        }
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        self.quantitySelectedArr.removeAll()
        if cartIdArr.count > 0 {
            for i in 0..<self.cartIdArr.count {
                let cartId = self.cartIdArr[i]
                for j in 0..<self.listData.count {
                    if cartId == self.listData[j].id {
                        self.quantitySelectedArr.append(self.quantityTFArr[j])
                    }
                }
            }
            if iconClick == true {
                self.goToRefundFinalScreen()
            } else {
                self.showAlert(title: "Terms and Condtions", message: "Please tick the read and agree to the return conditions")
            }
        } else {
            self.showAlert(title: "Alert", message: "Please select product item from list")
        }
    }
    
    func goToRefundFinalScreen() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "RefundFinalVC") as! RefundFinalVC
            vc.reasonStr = self.selectedReason
            if let id = orderData?.id {
                let idStr = String(id)
                vc.orderId = idStr
            }
            vc.cartIdArr = self.cartIdArr
            vc.quantityArr = self.quantitySelectedArr
            vc.userId = self.userId
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RefundFinalVC") as! RefundFinalVC
            vc.reasonStr = self.selectedReason
            if let id = orderData?.id {
                let idStr = String(id)
                vc.orderId = idStr
            }
            vc.cartIdArr = self.cartIdArr
            vc.quantityArr = self.quantitySelectedArr
            vc.userId = self.userId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
}

extension RefundProductsVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RefundProductsTVCell", for: indexPath) as! RefundProductsTVCell
        
        cell.dateLbl.text = orderData?.date
        cell.proNameLbl.text = listData[indexPath.row].proName
        let quantity = self.quantityTFArr[indexPath.row]
            cell.quantityTF.text = String(quantity)
            cell.quantityTF.delegate = self
            cell.quantityTF.tag = indexPath.row
            cell.quantityTF.addTarget(self, action: #selector(RefundProductsVC.textFieldDidChange(_:)),
            for: .editingChanged)
        
        cell.priceLbl.text = listData[indexPath.row].totalPrice
        
        cell.minusBtnOutlet.tag = indexPath.row
        cell.plusBtnOutlet.tag = indexPath.row
        cell.minusBtnOutlet.addTarget(self, action: #selector(minusBtnAction(_:)), for: .touchUpInside)
        cell.plusBtnOutlet.addTarget(self, action: #selector(plusBtnAction(_:)), for: .touchUpInside)
        cell.checkBtn.tag = indexPath.row
        cell.checkBtn.addTarget(self, action: #selector(checkBtnCellAction(_:)), for: .touchUpInside)
        
        if let refund = listData[indexPath.row].refund {
            if refund == 0 {
                cell.checkView.isHidden = false
                cell.requestLbl.text = ""
                cell.plusBtnOutlet.isEnabled = true
                cell.minusBtnOutlet.isEnabled = true
            } else {
                cell.checkView.isHidden = true
                cell.requestLbl.text = "Already Requested"
                cell.plusBtnOutlet.isEnabled = false
                cell.minusBtnOutlet.isEnabled = false
            }
        }
        
        if cartIdArr.contains(listData[indexPath.row].id ?? 0) {
            cell.checkBtn.setImage(UIImage(named: "rightTick"), for: .normal)
        } else {
            cell.checkBtn.setImage(UIImage(named: ""), for: .normal)
        }
        
        
        return cell
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        print(textField.text as Any)
    }
    
    @objc func checkBtnCellAction(_ sender: UIButton) {
        let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! RefundProductsTVCell
        if selectedIndex.contains(sender.tag) {
            for i in 0..<selectedIndex.count {
                let delete = selectedIndex[i]
                if delete == sender.tag {
                    selectedIndex.remove(at: i)
                    //quantityArr.remove(at: i)
                    cartIdArr.remove(at: i)
                    cell.checkBtn.setImage(UIImage(named: ""), for: .normal)
                    return
                }
            }
            
                
            //remove right clicked
            
            
        } else {
            //add index and change button image to right clicked
            selectedIndex.append(sender.tag)
            let cartId = cell.quantityTF.text!
            let id: Int = Int(cartId)!
            //quantityArr.append(id)
            cartIdArr.append(listData[sender.tag].id!)
            
            cell.checkBtn.setImage(UIImage(named: "rightTick"), for: .normal)
        }
        
    }
    
    @objc func minusBtnAction(_ sender: UIButton) {
        
        print("minus btn press")
        let quantity = listData[sender.tag].quantity
        let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! RefundProductsTVCell
        let quaTxt = Int(cell.quantityTF.text ?? "1")
        if quaTxt == 1 {
            cell.quantityTF.text = "1"
        } else if quaTxt! >= 1 || quaTxt! <= quantity ?? 1{
            let out = quaTxt! - 1
            cell.quantityTF.text = String(quaTxt! - 1)
                        
            self.quantityTFArr[sender.tag] = out
                        
        } else {
            cell.quantityTF.text = "1"
        }
    }
    
    @objc func plusBtnAction(_ sender: UIButton) {
        
        print("plus btn press")
        let quantity = listData[sender.tag].quantity
        let cell = self.tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! RefundProductsTVCell
        let quaTxt = Int(cell.quantityTF.text ?? "1")
        if quaTxt! <= quantity ?? 1{
            if quaTxt == quantity {
                self.showAlert(title: "You can not add more than \(quaTxt ?? 0) units", message: "")
            } else {
                let out = (quaTxt! + 1)
                cell.quantityTF.text = String(out)
                                
                self.quantityTFArr[sender.tag] = out
                
            }
        } else {
            cell.quantityTF.text = "1"
        }
    }
    
    
    
}

extension RefundProductsVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Edited text filed value is:- ", textField.text!)
    }
    
}
