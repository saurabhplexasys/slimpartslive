//
//  SearchKTypeModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 31/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct SearchKTypeModel {
    let makeCode: String?
    let makeName: String?
    let modelCode: String?
    let modelName: String?
    let typeCode: String?
    let typeName: String?
    let kTypeCode: String?
}
