//
//  ModelModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 30/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation


struct ModelModel {
    var modelCode: String?
    var modelName: String?
}

