//
//  ModelViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 30/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import Alamofire


class ModelViewModel {
    
    func getModelData(makeCode: String, completion : @escaping(Bool, String, [ModelModel]) -> Void) {
        
        AppInstance.showLoader()
        
        var modelData = [ModelModel]()
        modelData.removeAll()
        
        let url = Slimparts.baseURL + Slimparts.makeModelURL + makeCode
                
        AF.request(url, method: .get).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["ResponseCode"] as? String, let message = responseObj["ResponseText"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem", modelData)
                    return
                }
                if status == "1" {
                    let _data = responseObj["data"] as? [[String: Any]]
                    if !_data!.isEmpty {
                        for data in _data! {
                            let _modelCode = data["modelcode"] as? String
                            let _modelName = data["modelname"] as? String
                            
                            let md = ModelModel(modelCode: _modelCode ?? "", modelName: _modelName ?? "")
                            modelData.append(md)
                        }
                        AppInstance.hideLoader()
                        completion(true, message, modelData)
                    } else {
                        AppInstance.hideLoader()
                        completion(false, message, modelData)
                    }
                } else {
                    AppInstance.hideLoader()
                    completion(false, message, modelData)
                }
            case .failure(let error): 
                AppInstance.hideLoader()
                completion(false, "serverProblem", modelData)
            }
        }
    }
    
}
