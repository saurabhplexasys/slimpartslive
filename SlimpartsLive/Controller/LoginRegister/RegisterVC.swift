//
//  RegisterVC.swift
//  Slimparts
//
//  Created by Dharmesh Kothari on 21/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class RegisterVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var firstNameTF: ShakingTextField!
    @IBOutlet weak var lastNameTF: ShakingTextField!
    @IBOutlet weak var emailTF: ShakingTextField!
    @IBOutlet weak var passwordTF: ShakingTextField!
    @IBOutlet weak var confPassTF: ShakingTextField!
    
    var registerUser = RegisterViewModel()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.isHidden = true
        self.textFieldDelegateSelf()
        // Do any additional setup after loading the view.
    }
    
    func textFieldDelegateSelf() {
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        emailTF.delegate = self
        passwordTF.delegate = self
        confPassTF.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func registerBtnAction(sender: UIButton) {
        self.registrationValidation()
    }
    
    func registrationValidation() {
        if firstNameTF.text!.count > 0 {
            if lastNameTF.text!.count > 0 {
                guard let emailT = self.emailTF.text , self.emailTF.text?.count != 0 else {
                    self.showAlert(title: "Alert", message: "Please enter your email address")
                    return
                }
                if isValidEmail(emailID: emailT) == false {
                    self.showAlert(title: "Alert", message: "Please enter valid email address")
                } else {
                    if passwordTF.text!.count > 5 {
                        if passwordTF.text == confPassTF.text {
                            
                            // call Register API here
                            self.registerApiCall()
                            
                        } else {
                            self.showAlert(title: "Alert", message: "both password field must be matched")
                        }
                    } else {
                        self.showAlert(title: "Alert", message: "password length must be 6 alphanumeric")
                    }
                }
            } else {
                self.showAlert(title: "Alert", message: "enter last name")
            }
        } else {
            self.showAlert(title: "Alert", message: "enter first name")
        }
    }
    
    @IBAction func cancelBack(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func registerApiCall() {
        registerUser.getRegisterData(firstName: firstNameTF.text!, lastName: lastNameTF.text!, email: emailTF.text!, password: passwordTF.text!, confPassword: confPassTF.text!, auth: Slimparts.authKey) { (_isTrue, _message, _regUser) in
            if _isTrue {
                let data = _regUser.data
                let userName = (data?.first_name ?? "") + " " + (data?.last_name ?? "")
                KeychainWrapper.standard.set(data?.user_id ?? "", forKey: "userID")
                KeychainWrapper.standard.set(userName, forKey: "userName")
                KeychainWrapper.standard.set(data?.email ?? "", forKey: "email")
                KeychainWrapper.standard.set(data?.mobile ?? "", forKey: "mobile")
                self.alertViewCall(message: _message)
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    func alertViewCall(message: String) {
        let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
        let okAct = UIAlertAction(title: "OK", style: .default) { (action) in
            self.moveToHome()
        }
        alert.addAction(okAct)
        self.present(alert, animated: true, completion: nil)
    }
    
    func moveToHome() {
        let homeVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController

        self.revealViewController()?.pushFrontViewController(homeVC, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == firstNameTF {
            firstNameTF.shake()
        }
        if textField == lastNameTF {
            lastNameTF.shake()
        }
        if textField == emailTF {
            emailTF.shake()
        }
        if textField == passwordTF {
            passwordTF.shake()
        }
        if textField == confPassTF {
            confPassTF.shake()
        }
    }
   

}
