//
//  PlaceOrderViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 24/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class PlaceOrderViewModel {
    
    func placeFinalOrderApiCall(userId: String, url: String, payData: PaymentModel, cartList: ShoppingCartModel, userDetails: UserDetailsModel, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        let header: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let cart = cartList.cartList
        var cartFinalArr = [String]()
        for item in 0..<cart.count {
            if let id = cart[item].id {
                cartFinalArr.append(String(id))
            }
        }
        
        let addDetails = userDetails.addressDetail
        var bilAdd = ""
        var bilHouseNo = ""
        var bilCity = ""
        var bilPostCode = ""
        var bilId = 0
        var shiAdd = ""
        var shiHouseNo = ""
        var shiCity = ""
        var shiPostCode = ""
        var shiId = 0
        
        for item in 0..<addDetails!.count {
            let addType = addDetails![item].addressType
            if addType == "billing" {
                bilAdd = addDetails![item].address!
                bilHouseNo = addDetails![item].houseNo!
                bilCity = addDetails![item].city!
                bilPostCode = addDetails![item].postCode!
                bilId = addDetails![item].addressId!
            } else {
                shiAdd = addDetails![item].address!
                shiHouseNo = addDetails![item].houseNo!
                shiCity = addDetails![item].city!
                shiPostCode = addDetails![item].postCode!
                shiId = addDetails![item].addressId!
            }
        }
        
        let addressFinal = ["billing_address": bilAdd,
                            "billing_house_number": bilHouseNo,
                            "billing_city": bilCity,
                            "billing_postcode": bilPostCode,
                            "billing_id": String(bilId),
                            "shipping_address": shiAdd,
                            "shipping_house_number": shiHouseNo,
                            "shipping_city": shiCity,
                            "shipping_postcode": shiPostCode,
                            "shipping_id": String(shiId)] as [String : Any]
        
        let parameter: [String: Any] = ["user_id": userId,
                         "payment_id": payData.payId!,
                         "order_num": payData.payOrderNo!,
                         "tax": cartList.tax!,
                         "subtotal": cartList.subtotal!,
                         "shipping": cartList.shipping!,
                         "total": cartList.total!,
                         "cartlist": cartFinalArr,
                         "address": addressFinal] as [String : Any]
        
        
        AF.request(url, method: .post, parameters: parameter, headers: header).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["status"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! try after some time")
                    return
                }
                if status == "success" {
                    let message = responseObj["response"] as? String
                    AppInstance.hideLoader()
                    completion(true, message ?? "Order has been placed successfully.")
                } else {
                    let message = responseObj["response"] as? String
                    AppInstance.hideLoader()
                    completion(false, message ?? "")
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! try after some time")
            }
        }
        
    }
}


