//
//  CheckoutOrderVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 12/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class CheckoutOrderVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    
    @IBOutlet weak var paymentBtn: UIButton!
    @IBOutlet weak var termConBtn: UIButton!
    
    
    
    var iconClickPayment = true {
        didSet {
            if iconClickPayment {
                let image = UIImage(named: "rightTick")
                paymentBtn.setImage(image, for: .normal)
            } else {
                let image = UIImage(named: "")
                paymentBtn.setImage(image, for: .normal)
            }
        }
    }
    var iconClickTermCon = true {
        didSet {
            if iconClickTermCon {
                let image = UIImage(named: "rightTick")
                termConBtn.setImage(image, for: .normal)
            } else {
                let image = UIImage(named: "")
                termConBtn.setImage(image, for: .normal)
            }
        }
    }
    
    var cartList: ShoppingCartModel?
    var userDetails: UserDetailsModel?
    
    var payHit = PaymentViewModel()
    var payData: PaymentModel?
    
    var userId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        tableView.delegate = self
        tableView.dataSource = self
        iconClickPayment = false
        iconClickTermCon = false
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            userId = userid
        }
        self.updateOrderDetails()
        
        // Do any additional setup after loading the view.
    }
    
    func updateOrderDetails() {
        
        if let subtotal = cartList?.subtotal {
            self.subTotalLbl.text = subtotal
        }
        if let tax = cartList?.tax {
            self.taxLbl.text = tax
        }
        if let shiping = cartList?.shipping {
            self.shippingLbl.text = shiping
        }
        if let total = cartList?.total {
            self.totalLbl.text = total
        }
        if let list = cartList?.cartList {
            if list.count > 1 {
                self.tableViewHeightContraint.constant = CGFloat(44 * list.count)
            } else {
                self.tableViewHeightContraint.constant = 44
            }
            self.tableView.reloadData()
        }
        
    }
    
    @IBAction func iconPaymentAction(_ sender: UIButton) {
        iconClickPayment = !iconClickPayment
    }
    
    @IBAction func iconTermConAction(_ sender: UIButton) {
        iconClickTermCon = !iconClickTermCon
    }
    
    
    @IBAction func placeOrderBtnAction(_ sender: UIButton) {
        
        if iconClickPayment == true {
            if iconClickTermCon == true {
                //go to next screen for payment
                self.showAlertAction(title: "Place Order", message: "Are you sure, you want to place the order", alertStyle: .alert, actionTitles: ["Continue", "Cancel"], actionStyles: [.default, .cancel], actions: [{_ in
                        // Continue payment
                        self.paymentCheckOut()
                    }, {_ in
                        print("Cancel btn clicked")
                    }])
                
            } else {
                self.showAlert(title: "Alert", message: "The checkout terms must be accepted.")
            }
        } else {
            self.showAlert(title: "Alert", message: "The payment method field is required.")
        }
        
    }
    
    func paymentCheckOut() {
        payHit.paymentApiCall(userId: userId, total: (cartList?.total)!) { (_isTrue, _message, _data) in
            if _isTrue {
                
                // payment success
                self.payData = _data
                //here you can perform place order api
                self.moveToPaymentWeb()
                
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    func moveToPaymentWeb() {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PaymentWebVC") as! PaymentWebVC
            vc.payData = self.payData
            vc.cartList = self.cartList
            vc.userDetails = self.userDetails
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentWebVC") as! PaymentWebVC
            vc.payData = self.payData
            vc.cartList = self.cartList
            vc.userDetails = self.userDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    

}

extension CheckoutOrderVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (cartList?.cartList.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckoutOrderTVCell", for: indexPath) as! CheckoutOrderTVCell
        
        if let name = cartList?.cartList[indexPath.row].name {
            if let quantity = cartList?.cartList[indexPath.row].count {
                let data = "\(name) x \(quantity)"
                cell.proNameQuantityLbl.text = data
            }
        }
        if let price = cartList?.cartList[indexPath.row].price {
            cell.priceLbl.text = price
        }
        
        
        return cell
    }
    
}
