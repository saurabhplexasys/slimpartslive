//
//  LoginVC.swift
//  Slimparts
//
//  Created by Dharmesh Kothari on 19/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var emailTxt: ShakingTextField!
    @IBOutlet weak var passwordTxt: ShakingTextField!
    @IBOutlet weak var eyeIconBtn: UIButton!
    @IBOutlet weak var loginBtnOutlet: UIButton!
    
    var iconClick = true {
        didSet {
            if iconClick {
                let eyeLock = UIImage(named: "pwLock")
                eyeIconBtn.setImage(eyeLock, for: .normal)
            } else {
                let eyeUnlock = UIImage(named: "pwUnlock")
                eyeIconBtn.setImage(eyeUnlock, for: .normal)
            }
        }
    }
    
    var viewModelLogin = loginViewModel()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.setMenuBarBtn()
        emailTxt.delegate = self
        passwordTxt.delegate = self
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    @IBAction func loginBtnAction(sender: UIButton) {
        self.loginWithValidation()
    }
    
    func loginWithValidation() {
        guard let emailT = self.emailTxt.text , self.emailTxt.text?.count != 0 else {
            self.showAlert(title: "Alert", message: "Please enter your email address")
            return
        }
        if isValidEmail(emailID: emailT) == false {
            self.showAlert(title: "Alert", message: "Please enter valid email address")
        } else {
            if passwordTxt.text!.count >= 6 {
                // call login API here
                print("please call here login API")
                viewModelLogin.getLoginData(email: self.emailTxt.text!, password: self.passwordTxt.text!, auth: Slimparts.authKey) { (_isTrue, _message, _loginData) in
                    if _isTrue {
                        print("Login data is:- ", _loginData)
                        
                        let data = _loginData.data
                        let userName = (data?.first_name ?? "") + " " + (data?.last_name ?? "")
                        KeychainWrapper.standard.set(data?.user_id ?? "", forKey: "userID")
                        KeychainWrapper.standard.set(userName, forKey: "userName")
                        KeychainWrapper.standard.set(data?.email ?? "", forKey: "email")
                        KeychainWrapper.standard.set(data?.mobile ?? "", forKey: "mobile")
                        DispatchQueue.main.async {
                            self.alertViewCall(message: _message)
                        }
                    } else {
                        self.showAlert(title: "Alert", message: _message)
                    }
                }
                
            } else {
                self.showAlert(title: "Alert", message: "please enter your password and length must be 6 alphanumeric")
            }
        }
    }
    
    func alertViewCall(message: String) {
        let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
        let okAct = UIAlertAction(title: "OK", style: .default) { (action) in
            self.moveToHome()
        }
        alert.addAction(okAct)
        self.present(alert, animated: true, completion: nil)
    }
    
    func moveToHome() {
        let homeVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController

        self.revealViewController()?.pushFrontViewController(homeVC, animated: true)
    }
    
    @IBAction func eyeBtnAction(sender: UIButton) {
        iconClick = !iconClick
        passwordTxt.isSecureTextEntry = iconClick
    }
    
    @IBAction func forgotPassBtnAction(sender: UIButton) {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ForgotPasswordVC") as! ForgotPasswordVC
            self.present(vc, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func createAcctBtnAction(sender: UIButton) {
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "RegisterVC") as! RegisterVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func procWithOutAcctBtnAction(sender: UIButton) {
        self.moveToHome()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == emailTxt {
            emailTxt.shake()
        }
        if textField == passwordTxt {
            passwordTxt.shake()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
