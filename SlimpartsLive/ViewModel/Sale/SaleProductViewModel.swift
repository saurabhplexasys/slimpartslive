//
//  SaleProductViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 05/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class SaleProductViewModel {
    
    func getSaleProductList(catId: Int, subCatId: Int, completion: @escaping(Bool, String, [SaleProductModel]) -> Void) {
        
        AppInstance.showLoader()
        
        let catId = catId
        let subCatId = "\(subCatId)"
        let link = "\(catId)/\(subCatId)"
        
        var saleProduct = [SaleProductModel]()
        saleProduct.removeAll()
        
        let url = Slimparts.baseURL + Slimparts.getSaleProductListURL + link
                
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                
                guard  let responseObj = response.value as? [[String: Any]] else {
                    AppInstance.hideLoader()
                    completion(false, "Server Side Issue! try after some time", saleProduct)
                    return
                }
                
                if !responseObj.isEmpty {
                    
                    for respo in responseObj {
                        
                        let _proId = respo["id"] as? Int
                        let _proUniqId = respo["unique_id"] as? String
                        let _proName = respo["name"] as? String
                        let _proPrice = respo["price"] as? String
                        let _proDesc = respo["description"] as? String
                        let _proImg = respo["image"] as? String
                        let _proQuantity = respo["quantity"] as? Int
                        let _status = respo["status"] as? String
                        let _catId = respo["catID"] as? Int
                        let _subCatId = respo["sub_catID"] as? Int
                        let _brand = respo["brand"] as? String
                        
                        let data = SaleProductModel(proId: _proId ?? 0, proUniqId: _proUniqId ?? "", proName: _proName ?? "", proPrice: _proPrice ?? "", proDesc: _proDesc ?? "", proImg: _proImg ?? "", proQuantity: _proQuantity ?? 0, status: _status ?? "", catId: _catId ?? 0, subCatId: _subCatId ?? 0, brand: _brand ?? "")
                        
                        saleProduct.append(data)
                    }
                    AppInstance.hideLoader()
                    completion(true, "", saleProduct)
                    
                } else {
                    AppInstance.hideLoader()
                    completion(false, "Data Not Found", saleProduct)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! try after some time", saleProduct)
                
            }
            
        }
        
        
        
        
    }
    
}
