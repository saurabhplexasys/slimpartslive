//
//  ViewOrderHistoryViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 01/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class ViewOrderHistoryViewModel {
    
    func viewOrderHistoryApiCall(orderId: String, userId: String, completion: @escaping(Bool, String, ViewOrderModel)-> Void) {
        
        AppInstance.showLoader()
        
        var userData = UserViewOrderModel(id: 0, firstName: "", lastName: "", email: "", mobile: "", profileImg: "")
        var orderData = OrderViewOrderModel(id: 0, date: "", orderNo: "", subTotal: "", shipping: "", tax: "", totalPrice: "", status: "")
        var cartData = [CartListViewOrderModel]()
        cartData.removeAll()
        var addressData = [AddressViewOrderModel]()
        addressData.removeAll()
        
        var isGetData: Bool = false
        
        var viewData = ViewOrderModel(user: userData, orders: orderData, cartList: cartData, address: addressData)
        
        let getID = "\(orderId)/\(userId)"
        
        let url = Slimparts.baseURL + Slimparts.viewOrderHistoryURL + getID
        
        AF.request(url, method: .get).responseJSON { (response) in
            switch response.result {
                
            case .success:
                guard  let responseObj = response.value as? [String: Any] else {
                    
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time", viewData)
                    return
                }
                
                let _userData = responseObj["user"] as? [[String: Any]]
                if !_userData!.isEmpty {
                    for user in _userData! {
                        let _id = user["id"] as? Int
                        let _firstName = user["first_name"] as? String
                        let _lastName = user["last_name"] as? String
                        let _email = user["email"] as? String
                        let _mobile = user["mobile"] as? String
                        let _profImg = user["profile_image"] as? String
                        
                        isGetData = true
                        
                        userData = UserViewOrderModel(id: _id ?? 0, firstName: _firstName ?? "", lastName: _lastName ?? "", email: _email ?? "", mobile: _mobile ?? "", profileImg: _profImg ?? "")
                    }
                }
                
                let _orderData = responseObj["orders"] as? [[String: Any]]
                if !_orderData!.isEmpty {
                    
                    for order in _orderData! {
                        let _id = order["id"] as? Int
                        let _date = order["created_at"] as? String
                        let _orderNo = order["order_num"] as? String
                        let _subtotal = order["subtotal"] as? String
                        let _shipping = order["shipping"] as? String
                        let _tax = order["tax"] as? String
                        let _total = order["total"] as? String
                        let _status = order["status"] as? String
                        
                        isGetData = true
                        
                        orderData = OrderViewOrderModel(id: _id ?? 0, date: _date ?? "", orderNo: _orderNo ?? "", subTotal: _subtotal ?? "", shipping: _shipping ?? "", tax: _tax ?? "", totalPrice: _total ?? "", status: _status ?? "")
                        
                    }
                }
                
                let _cartData = responseObj["cartlist"] as? [[String: Any]]
                if !_cartData!.isEmpty {
                    for cart in _cartData! {
                        let _id = cart["id"] as? Int
                        let _proId = cart["product_id"] as? Int
                        let _proName = cart["product_name"] as? String
                        let _totalPrice = cart["total_price"] as? String
                        let _quantity = cart["quantity"] as? Int
                        let _proImage = cart["product_image"] as? String
                        let _proCode = cart["product_code"] as? String
                        let _supCode = cart["supcode"] as? String
                        
                        let data = CartListViewOrderModel(id: _id ?? 0, productId: _proId ?? 0, productNAme: _proName ?? "", price: _totalPrice ?? "", quantity: _quantity ?? 0, proImg: _proImage ?? "", proCode: _proCode ?? "", supCode: _supCode ?? "")
                        
                        isGetData = true
                        
                        cartData.append(data)
                    }
                }
                
                let _addressData = responseObj["addressDetail"] as? [[String: Any]]
                if !_addressData!.isEmpty {
                    for address in _addressData! {
                        let _id = address["id"] as? Int
                        let _address = address["address"] as? String
                        let _city = address["city"] as? String
                        let _postcode = address["postcode"] as? String
                        let _houseNo = address["house_number"] as? String
                        let _addType = address["address_type"] as? String
                        
                        let data = AddressViewOrderModel(id: _id ?? 0, address: _address ?? "", city: _city ?? "", postCode: _postcode ?? "", houseNo: _houseNo ?? "", addType: _addType ?? "")
                        
                        isGetData = true
                        
                        addressData.append(data)
                        
                    }
                }
                
                if isGetData == true {
                    
                    viewData = ViewOrderModel(user: userData, orders: orderData, cartList: cartData, address: addressData)
                    AppInstance.hideLoader()
                    completion(true, "", viewData)
                } else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time", viewData)
                }
                
            case .failure:
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time", viewData)
            }
        }
        
        
        
        
    }
    
}
