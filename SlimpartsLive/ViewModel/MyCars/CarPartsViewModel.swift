//
//  CarPartsViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 22/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class CarPartsViewModel {
    
    func getCarProductAPiCall(typeCode: String, menuCode: String, partCode: String, completion: @escaping(Bool, String, [CarPartsModel]) -> Void) {
        
        AppInstance.showLoader()
        
        var carProduct = [CarPartsModel]()
        carProduct.removeAll()
        
        let subUrl = "v1/single-car-parts-price?typecode=\(typeCode)&menucode=\(menuCode)&partcode=\(partCode)"
        
        
        let url = Slimparts.baseURL + subUrl
        
        print("product list url:- ", url)
        
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
                
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["ResponseCode"] as? String, let message = responseObj["ResponseText"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time", carProduct)
                    return
                }
                if status == "1" {
                    let _data = responseObj["data"] as? [[String: Any]]
                    
                    if _data != nil {
                        
                        for data in _data! {
                            
                            let _art = data["art"] as? String
                            let _part = data["part"] as? String
                            let _sup = data["sup"] as? String
                            let _refrem = data["refrem"] as? String
                            let _inkrem = data["lnkrem"] as? String
                            let _price = data["prijs"] as? String
                            let _priceValue = data["prijsvalue"] as? String
                            let _btw = data["btw"] as? String
                            let _btwValue = data["btwvalue"] as? String
                            let _extraFee = data["extrafees"] as? String
                            let _extraFeeValue = data["extrafeesvalue"] as? String
                            let _stock = data["stock"] as? Int
                            let _pcode = data["pcode"] as? String
                            let _pic = data["pic"] as? String
                            let _supcode = data["supcode"] as? String
                            let _itemNo = data["itemnumber"] as? String
                            let _itemGroup = data["itemgroup"] as? String
                            let _addToCart = data["add_to_cart"] as? String
                            let _notifyMe = data["notify_me"] as? String
                            
                            let data = CarPartsModel(art: _art ?? "", part: _part ?? "", sup: _sup ?? "", refrem: _refrem ?? "", inkrem: _inkrem ?? "", price: _price ?? "", priceValue: _priceValue ?? "", btw: _btw ?? "", btwValue: _btwValue ?? "", extraFee: _extraFee ?? "", extraFeeValue: _extraFeeValue ?? "", stock: _stock ?? 0, pcode: _pcode ?? "", pic: _pic ?? "", supCode: _supcode ?? "", itemNo: _itemNo ?? "", itemGroup: _itemGroup ?? "", addToCart: _addToCart ?? "0", notifyMe: _notifyMe ?? "0")
                            
                            carProduct.append(data)
                        }
                        
                        AppInstance.hideLoader()
                        completion(true, message, carProduct)
                    } else {
                        AppInstance.hideLoader()
                        completion(false, "Data Not found", carProduct)
                    }
                } else {
                    AppInstance.hideLoader()
                    completion(false, message, carProduct)
                }
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time", carProduct)
                
            }
        }
        
        
        
        
    }
    
    
}
