//
//  LoginViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 29/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class loginViewModel {
    
    weak var vc: LoginVC?
    
    func getLoginData(email: String, password: String, auth: String, completion : @escaping(Bool, String, LoginModel) -> Void) {
        
        AppInstance.showLoader()
        
        let header: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        var loginUser : LoginModel?
        
        let url = Slimparts.baseURL + Slimparts.loginURL
        let parameter = ["email": email, "password": password, "auth_key": auth]
        
        AF.request(url, method: .post, parameters: parameter, encoder: JSONParameterEncoder.default, headers: header).response { (response) in
            if let data = response.data {
                do {
                    let userRes = try JSONDecoder().decode(LoginModel.self, from: data)
                    loginUser = userRes
                    if loginUser?.status == "success" {
                        AppInstance.hideLoader()
                        completion(true, loginUser?.message ?? "", loginUser!)
                    } else {
                        AppInstance.hideLoader()
                        completion(false, loginUser?.message ?? "", loginUser!)
                    }
                } catch let err {
                    
                    AppInstance.hideLoader()
                    completion(false, err.localizedDescription, loginUser!)
                }
            } else {
                AppInstance.hideLoader()
                completion(false, "", loginUser!)
            }
        }
        
    }
    
    
}
