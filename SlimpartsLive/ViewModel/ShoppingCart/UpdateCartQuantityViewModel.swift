//
//  UpdateCartQuantity.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 19/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class UpdateCartQuantityViewModel {
    
    func updateCartQuantityAPICall(userId: String, cartId: String, quantity: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        let url = Slimparts.baseURL + Slimparts.updateQuantityCartURL
        let parameter = ["user_id": userId, "cart_id": cartId, "count": quantity]
        
        AF.request(url, method: .post, parameters: parameter).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["status"] as? String, let message = responseObj["msg"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! try after some time")
                    return
                }
                if status == "success" {
                    AppInstance.hideLoader()
                    completion(true, message)
                } else {
                    AppInstance.hideLoader()
                    completion(false, message)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! try after some time")
            }
        }
        
    }
    
}
