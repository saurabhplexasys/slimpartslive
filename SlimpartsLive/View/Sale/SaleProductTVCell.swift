//
//  SaleProductTVCell.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 05/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class SaleProductTVCell: UITableViewCell {

    @IBOutlet weak var proNameLbl: UILabel!
    @IBOutlet weak var proCodeLbl: UILabel!
    @IBOutlet weak var proPriceLbl: UILabel!
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var viewDetailBtnOutlet: UIButton!
    @IBOutlet weak var quantityView: BorderView!
    @IBOutlet weak var quantityTF: UITextField!
    @IBOutlet weak var minusBtnOutlet: UIButton!
    @IBOutlet weak var plusBtnOutlet: UIButton!
    @IBOutlet weak var addToCartView: BorderView!
    @IBOutlet weak var addToCartBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
