//
//  ChangePasswordVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 28/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class ChangePasswordVC: UIViewController {

    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var oldPassTF: UITextField!
    @IBOutlet weak var newPassTF: UITextField!
    @IBOutlet weak var confPassTF: UITextField!
    
    var changePass = ChangePasswordViewModel()
    var userId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.setMenuBarBtn()
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
            print("user id:- ", self.userId)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    @IBAction func updateBtnAction(sender: UIButton) {
        print("update btn clicked")
        self.passwordValidate()
    }
    
    func passwordValidate() {
        if oldPassTF.text!.count > 1  {
            if newPassTF.text!.count >= 6  {
                if confPassTF.text!.count > 1 {
                    if newPassTF.text == confPassTF.text {
                        
                        // hit change password api
                        self.changePasswordAPiCall()
                        
                    } else {
                        self.showAlert(title: "Alert", message: "both new password and confirm password should be same")
                    }
                } else {
                    self.showAlert(title: "Alert", message: "please set your confirm password")
                }
            } else {
                self.showAlert(title: "Alert", message: "please set your new password and lenght must be 6 alphanumeric")
            }
        } else {
            self.showAlert(title: "Alert", message: "please enter your old password")
        }
    }
    
    func changePasswordAPiCall() {
        changePass.changePasswordAPiCall(oldPass: self.oldPassTF.text ?? "", newPass: self.newPassTF.text ?? "", confPass: self.confPassTF.text ?? "", userId: userId) { (_isTrue, _message) in
            if _isTrue {
                self.showAlert(title: _message, message: "")
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
 
}
