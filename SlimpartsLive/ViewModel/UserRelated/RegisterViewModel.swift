//
//  RegisterViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 29/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import UIKit

import Alamofire

class RegisterViewModel {
    
    weak var vc: RegisterVC?
    
    func getRegisterData(firstName: String, lastName: String, email: String, password: String, confPassword: String, auth: String, completion : @escaping(Bool, String, RegisterModel) -> Void) {
        
        AppInstance.showLoader()
        
        let header: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        var registerUser : RegisterModel?
        
        let url = Slimparts.baseURL + Slimparts.registerURL
        let parameter = ["first_name": firstName, "last_name": lastName, "email": email, "password": password, "password_confirmation": confPassword, "auth_key": auth]
        
        AF.request(url, method: .post, parameters: parameter, encoder: JSONParameterEncoder.default, headers: header).response { (response) in
            if let data = response.data {
                do {
                    let userRes = try JSONDecoder().decode(RegisterModel.self, from: data)
                    registerUser = userRes
                    if registerUser?.status == "success" {
                        AppInstance.hideLoader()
                        completion(true, registerUser?.message ?? "", registerUser!)
                    } else {
                        AppInstance.hideLoader()
                        completion(false, registerUser?.message ?? "", registerUser!)
                    }
                } catch let err {
                    AppInstance.hideLoader()
                    completion(false, err.localizedDescription, registerUser!)
                }
            } else {
                AppInstance.hideLoader()
                completion(false, "", registerUser!)
            }
        }
        
    }
    
    
}
