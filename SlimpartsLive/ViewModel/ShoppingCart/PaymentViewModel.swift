//
//  PaymentViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 22/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class PaymentViewModel {
    
    func paymentApiCall(userId: String, total: String, completion: @escaping(Bool, String, PaymentModel) -> Void) {
        
        AppInstance.showLoader()
        let header: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let url = Slimparts.baseURL + Slimparts.paymentURL
        
        let parameter = ["user_id": userId, "total": total]
        
    var payData = PaymentModel(payId: "", payMethod: "", payStatus: "", payProfileID: "", payOrderNo: "", payCheckOutUrl: "")
        
        AF.request(url, method: .post, parameters: parameter, headers: header).responseJSON { (response) in
            
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["status"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! try after some time", payData)
                    return
                }
                if status == "success" {
                    let _payId = responseObj["payment_id"] as? String
                    let _payMethod = responseObj["method"] as? String
                    let _payStatus = responseObj["payment_status"] as? String
                    let _payProfileId = responseObj["profileId"] as? String
                    let _payOrderNo = responseObj["order_num"] as? String
                    let _checOutUrl = responseObj["checkouturl"] as? String
                    
                    payData = PaymentModel(payId: _payId ?? "", payMethod: _payMethod ?? "", payStatus: _payStatus ?? "", payProfileID: _payProfileId ?? "", payOrderNo: _payOrderNo ?? "", payCheckOutUrl: _checOutUrl)
                    
                    AppInstance.hideLoader()
                    completion(true, "Success", payData)

                } else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! try after some time", payData)
                }
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! try after some time", payData)
            }
            
        }
        
        
    }
    
}
