//
//  MyOrderViewAddressTVCell.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 02/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class MyOrderViewAddressTVCell: UITableViewCell {

    @IBOutlet weak var addressTypeLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
