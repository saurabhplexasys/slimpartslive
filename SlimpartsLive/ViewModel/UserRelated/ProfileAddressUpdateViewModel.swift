//
//  ProfileAddressUpdateViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 01/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class ProfileAddressUpdateViewModel {
    
    func updateAddressAPiCall(addressB: String, cityB: String, postcodeB: String, addressS: String, cityS: String, postcodeS: String, userId: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        
        let billing = ["address": addressB, "town_city": cityB, "postcode": postcodeB]
        let shipping = ["address": addressS, "town_city": cityS, "postcode": postcodeS]
        
        let url = Slimparts.baseURL + Slimparts.addUserAddressURL
        let parameter = ["user_id": userId, "b": billing, "s": shipping] as [String : Any]
        
        AF.request(url, method: .post, parameters: parameter).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let message = responseObj["msg"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem, please try again after some time")
                    return
                }
                
                AppInstance.hideLoader()
                completion(true, message)
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem, please try again after some time")
            }
        }
         
    }
    
}
