//
//  ForgotPasswordViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 25/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class ForgotPasswordViewModel {
    
    func forgotPasswordApiCall(email: String, authKey: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        let header: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let url = Slimparts.baseURL + Slimparts.forgotPasswordUrl
        
        let parameter = ["email": email, "auth_key": authKey]
        
        AF.request(url, method: .post, parameters: parameter, headers: header).responseJSON { (response) in
            switch response.result {
                
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["status"] as? String, let message = responseObj["message"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time")
                    return
                }
                if status == "success" {
                    AppInstance.hideLoader()
                    completion(true, message)
                } else {
                    AppInstance.hideLoader()
                    completion(false, message)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time")
            }
        }
    }
    
}
