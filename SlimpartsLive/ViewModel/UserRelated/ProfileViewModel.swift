//
//  ProfileViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 31/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class ProfileViewModel {
    
    func updateProfileAPI(firstName: String, lastName: String, mobile: String, userId: String, completion: @escaping(Bool, String, ProfileModel) -> Void) {
        
        AppInstance.showLoader()
        var profileData = ProfileModel(firstName: "", lastName: "", mobile: "")
        
        let url = Slimparts.baseURL + Slimparts.profileUpdateURL
        
        let parameter = ["first_name": firstName, "last_name": lastName, "mobile": mobile, "user_id": userId]
        
        AF.request(url, method: .post, parameters: parameter).responseJSON { (response) in
            switch response.result {
            case .success:
                let responseObj = response.value as? [String: Any]
                let status = responseObj!["ResponseCode"] as? String
                let message = responseObj!["msg"] as? String
                if status == "0" {
                    AppInstance.hideLoader()
                    completion(false, message ?? "Unable to update profile please try again.", profileData)
                    
                } else {
                    let _firstName = responseObj!["first_name"] as? String
                    let _lastName = responseObj!["last_name"] as? String
                    let _mobile = responseObj!["mobile"] as? String
                    
                    profileData = ProfileModel(firstName: _firstName ?? "", lastName: _lastName ?? "", mobile: _mobile ?? "")
                    AppInstance.hideLoader()
                    completion(true, "", profileData)
                }
            case .failure(let error): 
                AppInstance.hideLoader()
                completion(false, "serverProblem", profileData)
            }
        }
        
    }
    
    
}
