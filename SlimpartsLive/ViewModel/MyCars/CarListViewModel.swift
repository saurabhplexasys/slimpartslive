//
//  CarListViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 02/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class CarListViewModel {
    
    func getCarListAPiCall(userId: String, completion: @escaping (Bool, String, [CarListModel]) -> Void) {
        
        AppInstance.showLoader()
        let url = Slimparts.baseURL + Slimparts.userCarListURL + userId
        
        var carList = [CarListModel]()
        carList.removeAll()
        
        AF.request(url, method: .get).responseJSON { (response) in
            switch response.result {
            case .success:
                
                guard let responseObj = response.value as? [[String: Any]] else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem!, please try after some time", carList)
                    return
                }
                
                if !responseObj.isEmpty {
                    
                    for list in responseObj {
                        let _carId = list["id"] as? Int
                        let _make = list["brand"] as? String
                        let _model = list["modal"] as? String
                        let _engineType = list["type"] as? String
                        let _typeCode = list["typecode"] as? String
                        
                        let data = CarListModel(carId: _carId ?? 0, make: _make ?? "", model: _model ?? "", engineType: _engineType ?? "", typeCode: _typeCode ?? "")
                        carList.append(data)
                    }
                    AppInstance.hideLoader()
                    completion(true, "List of Car", carList)
                    
                } else {
                    AppInstance.hideLoader()
                    completion(false, "No car in list", carList)
                }
                
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time", carList)
            }
        }
        
        
    }
    
}
