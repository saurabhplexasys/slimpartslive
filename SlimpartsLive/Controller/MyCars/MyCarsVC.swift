//
//  MyCarsVCVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 28/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class MyCarsVC: UIViewController {
    
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var addBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var carList = [CarListModel]()
    var userCar = CarListViewModel()
    var deleteCar = DeleteCarViewModel()
    var searchCar = CarSearchByKtypeViewModel()
    
    var userId = ""
    var kTypeCode = ""
    var vaiCar = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.setMenuBarBtn()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        carList.removeAll()
        self.carlistAPiCall()
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func carlistAPiCall() {
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            self.userId = userid
        }
        userCar.getCarListAPiCall(userId: userId) { (_isTrue, _message, _carList) in
            if _isTrue {
                self.carList = _carList
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
    }
    
    
    @IBAction func addBtnAction(sender: UIBarButtonItem) {
        print("Add btn clicked")
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "AddCarVC") as! AddCarVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddCarVC") as! AddCarVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension MyCarsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCarsTVCell", for: indexPath) as! MyCarsTVCell
        
        cell.makeLbl.text = "Make: " + carList[indexPath.row].make!
        cell.modelLbl.text = "Model: " + carList[indexPath.row].model!
        cell.engineLbl.text = "Engine Type: " + carList[indexPath.row].engineType!
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let code = carList[indexPath.row].typeCode ?? ""
        self.getKtypeCodeApiCall(typeCode: code, carList: carList[indexPath.row])
        
    }
    
    func getKtypeCodeApiCall(typeCode: String, carList: CarListModel) {
        searchCar.getTypeCodeAPiCall(typeCode: typeCode) { (_isTrue, _message, _code) in
            if _isTrue {
                self.kTypeCode = _code
                if #available(iOS 13.0, *) {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyCarsCategoryVC") as! MyCarsCategoryVC
                    vc.makeName = carList.make ?? ""
                    vc.modelName = carList.model ?? ""
                    vc.engineType = carList.engineType ?? ""
                    vc.kTypeCode = self.kTypeCode
                    vc.vaiCar = self.vaiCar
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyCarsCategoryVC") as! MyCarsCategoryVC
                    vc.makeName = carList.make ?? ""
                    vc.modelName = carList.model ?? ""
                    vc.engineType = carList.engineType ?? ""
                    vc.kTypeCode = self.kTypeCode
                    vc.vaiCar = self.vaiCar
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                self.showAlert(title: _message, message: "")
            }
            
        }
    }
    
    
    @objc func deleteBtnAction(_ sender: UIButton) {
        print("Delete cell no \(sender.tag)")
        let code = carList[sender.tag].typeCode
        self.askForDeleteCar(typeCode: code!, tag: sender.tag)
    }
    
    func askForDeleteCar(typeCode: String, tag: Int) {
        let alert = UIAlertController(title: "Are you sure? you want to delete this car details", message: "", preferredStyle: .alert)
        let yesAct = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.deleteCarApiCall(typeCode: typeCode, tag: tag)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(cancel)
        alert.addAction(yesAct)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteCarApiCall(typeCode: String, tag: Int) {
        deleteCar.deleteCarApiCall(userId: userId, typeCode: typeCode) { (_isTrue, _message) in
            if _isTrue {
                self.showAlert(title: _message, message: "")
                self.carList.remove(at: tag)
                self.tableView.reloadData()
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
    }
    
}
