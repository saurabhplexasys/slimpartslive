//
//  ProfileModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 31/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct ProfileModel {
    let firstName: String?
    let lastName: String?
    let mobile: String?
}
