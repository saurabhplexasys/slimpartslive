//
//  RefundProductsTVCell.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 05/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class RefundProductsTVCell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var proNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityTF: UITextField!
    
    @IBOutlet weak var minusBtnOutlet: UIButton!
    @IBOutlet weak var plusBtnOutlet: UIButton!
    @IBOutlet weak var checkBtn: UIButton!
    
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var requestLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
