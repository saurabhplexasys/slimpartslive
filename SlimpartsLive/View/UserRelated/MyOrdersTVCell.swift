//
//  MyOrdersTVCell.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 24/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class MyOrdersTVCell: UITableViewCell {

    @IBOutlet weak var orderNoLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var paymentStatusLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var pdfBtn: UIButton!
    @IBOutlet weak var checkoutBtn: UIButton!
    @IBOutlet weak var refundBtn: UIButton!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
