//
//  MyCarsProductDetailsVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 21/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import SDWebImage
import SwiftKeychainWrapper

class MyCarsProductDetailsVC: UIViewController {
    
    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var backBtnOutlet: UIBarButtonItem!
    
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var proName: UILabel!
    @IBOutlet weak var proCode: UILabel!
    @IBOutlet weak var proDesc: UILabel!
    @IBOutlet weak var proPrice: UILabel!
    @IBOutlet weak var outStockBtnOutlet: UIButton!
    @IBOutlet weak var quantityTF: UITextField!
    @IBOutlet weak var quantityView: BorderView!
    
    var proDetails: CarPartsModel?
    
    var userId = ""
    var catId = ""
    var subCatId = ""
    var cartItem: [[String: AnyObject]] = []
    var isItemAdded = false
    var userAddCart = AddToCartSaleProductViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.navigationController?.navigationBar.tintColor = .white
        self.setMenuBarBtn()
        self.setProductDetailView()
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            userId = userid
        }
        
        // Do any additional setup after loading the view.
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func setProductDetailView() {
        
        self.proImg.sd_setImage(with: URL(string: proDetails?.pic ?? "defaultImage")) { (image, error, cache, url) in
            if error != nil {
                self.proImg.image = UIImage(named: "defaultImage")
            } else {
                self.proImg.image = image
            }
        }
        
        self.proName.text = proDetails?.sup ?? ""
        self.proCode.text = proDetails?.art ?? ""
        
        if let refrem = proDetails?.refrem {
            let refremPlus = "\u{2022}  " + refrem
            
            let refremFinal = refremPlus.replacingOccurrences(of: ";", with: "\n\u{2022} ")
            self.proDesc.text = refremFinal
        } else {
            self.proDesc.text = "-"
        }
    
        self.proPrice.text = proDetails?.price ?? ""
        self.quantityTF.text = "1"
        
        if let addCart = proDetails?.addToCart {
            if addCart == "1" {
                self.outStockBtnOutlet.setTitle("Add to Cart", for: .normal)
                self.outStockBtnOutlet.setTitleColor(UIColor.init(displayP3Red: 0/255, green: 84/255, blue: 147/255, alpha: 1), for: .normal)
                self.quantityView.isHidden = false
            } else {
                self.outStockBtnOutlet.setTitle("Notify Me!", for: .normal)
                self.outStockBtnOutlet.setTitleColor(UIColor.init(displayP3Red: 0/255, green: 116/255, blue: 0/255, alpha: 1), for: .normal)
                self.quantityView.isHidden = true
            }
        } else {
            self.outStockBtnOutlet.setTitle("Out Of Stock", for: .normal)
            self.quantityView.isHidden = true
        }
        
    }
    
    @IBAction func backBtnAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func minusBtnAction(_ sender: UIButton) {
        print("minus btn press")
        let quaTxt = Int(quantityTF.text ?? "1")
        if quaTxt == 1 {
            quantityTF.text = "1"
        } else if quaTxt! >= 1 {
            quantityTF.text = String(quaTxt! - 1)
        } else {
            quantityTF.text = "1"
        }
    }
    
    @IBAction func plusBtnAction(_ sender: UIButton) {
        print("plus btn press")
        let quaTxt = Int(quantityTF.text ?? "1")
        if quaTxt! >= 1 {
            let out = (quaTxt! + 1)
            quantityTF.text = String(out)
        } else {
            quantityTF.text = "1"
        }
    }
    
    
    @IBAction func addToCart(_ sender: UIButton) {
        
        if self.outStockBtnOutlet.titleLabel?.text == "Add to Cart" {
            // add to cart functionality
            print("****** Product add to cart *******")
            if userId == "" {
                //if not get userId then add product to locally
                
                var productImage = ""
                if let proImage = proDetails?.pic {
                    productImage = proImage
                }
                let quantity = self.quantityTF.text
                
                if let loadedCart = UserDefaults().array(forKey: "myCart") as? [[String: AnyObject]] {
                    for item in loadedCart {
                        let proId = "0"
                        if Int(proId) == item["proId"] as? Int {
                            isItemAdded = true
                        }
                    }
                }
                if isItemAdded == true {
                    isItemAdded = false
                    self.showAlert(title: "item already added", message: "")
                } else {
                    isItemAdded = false
                    cartItem.append(["proId": "1" as AnyObject, "proUniqId": proDetails?.supCode as AnyObject, "proName": proDetails?.sup as AnyObject, "proPrice": proDetails?.price as AnyObject, "proDesc": proDetails?.refrem as AnyObject, "proImg": productImage as AnyObject, "proQuantity": quantity as AnyObject, "status": "" as AnyObject, "catId": self.catId as AnyObject, "subCatId": self.subCatId as AnyObject, "brand": proDetails?.sup as AnyObject])
                    
                    UserDefaults.standard.set(cartItem, forKey: "cartItem")
                    
                    if let saveItem = UserDefaults.standard.array(forKey: "cartItem") {
                        print("Save item are :- ", saveItem)
                    }
                    
                    let count: String = String(cartItem.count)
                    
                    KeychainWrapper.standard.set(count, forKey: "notiCount")
                    
                    self.moveBackAfterSaveToCart(_message: "item save successfully")
                    
                    
                    //To fetch above cartItem from user default
                    //                if let loadedCart = NSUserDefaults().arrayForKey("myCart") as? [[String: AnyObject]] {
                    //                    print(loadedCart)  // [[price: 19.99, qty: 1, name: A], [price: 4.99, qty: 2, name: B]]"
                    //
                    //                    for item in loadedCart {
                    //                        print(item["name"] as! String)  // A, B
                    //                        print(item["price"] as! Double) // 19.99, 4.99
                    //                        print(item["qty"] as! Int)      // 1, 2
                    //                    }
                    //                }
                }
                
            } else {
                //hit add to cart api: only login user can add to cart directly to server
                print("*****Hit add to cart api*****")
                self.addToCartAPICAll()
                
            }
        } else {
            print("****** Product out of stock : Please notify me when available *******")
        }
        
    }
     
    func moveBackAfterSaveToCart(_message: String) {
        self.showAlertAction(title: "Success", message: _message, alertStyle: .alert, actionTitles: ["Ok"], actionStyles: [.default], actions: [{_ in
            self.navigationController?.popViewController(animated: true)
            }])
    }
    
    func addToCartAPICAll() {
        
        let price = proDetails?.price ?? "0.00"
        let priceDouble = Double(price)!
        let quan = self.quantityTF.text ?? "1"
        let quanInt = Int(quan)!
        
        let itemG = proDetails?.itemGroup ?? "0"
        let itemGInt: Int = Int(itemG)!
        
        userAddCart.addToCartSaleProductApiCall(name: proDetails?.sup ?? "", price: priceDouble, marginPrice: 0, quantity: quanInt, vehicleName: proDetails?.sup ?? "", image: proDetails?.pic ?? "", type: 0, uniCode: proDetails?.art ?? "", totalQuantity: proDetails?.stock ?? 0, supCode: proDetails?.supCode ?? "", userId: Int(self.userId)!, productId: "0", itemNo: proDetails?.itemNo ?? "", itemGroup: itemGInt, btw: proDetails?.btw ?? "", extraFee: proDetails?.extraFee ?? "") { (_isTrue, _message) in
            if _isTrue {
                self.moveBackAfterSaveToCart(_message: _message)
            } else {
                self.showAlert(title: _message, message: "")
            }
        }
        
    }
    
    
}
