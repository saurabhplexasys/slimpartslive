//
//  CarPartsModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 09/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation


struct CarPartsModel {
    var art: String?
    var part: String?
    var sup: String?
    var refrem: String?
    var inkrem: String?
    var price: String?
    var priceValue: String?
    var btw: String?
    var btwValue: String?
    var extraFee: String?
    var extraFeeValue: String?
    var stock: Int?
    var pcode: String?
    var pic: String?
    var supCode: String?
    var itemNo: String?
    var itemGroup: String?
    var addToCart: String?
    var notifyMe: String?
}
