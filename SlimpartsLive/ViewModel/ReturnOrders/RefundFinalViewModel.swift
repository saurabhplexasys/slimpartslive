//
//  RefundFinalViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 08/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class RefundFinalViewModel {
    
    func refundFinalApiCall(orderId: String, cartID: [Int], quntity: [Int], reason: String, userId: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        
        let header: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        let url = Slimparts.baseURL + Slimparts.refundFinalURL
        
        let parameter = ["orderId": orderId, "cartId": cartID, "qty": quntity, "reason": reason, "user_id": userId] as [String : Any]
        
        AF.request(url, method: .post, parameters: parameter, headers: header).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["status"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! please try after some time")
                    return
                }
                if status == "success" {
                    AppInstance.hideLoader()
                    completion(true, "Your Return Order Request registered Successfully.")
                } else {
                    AppInstance.hideLoader()
                    completion(false, "Sent Failed, please try after some time")
                }
                
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time")
            }
        }
        
    }
    
    
}
