//
//  DeleteCarViewModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 02/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import Foundation
import Alamofire

class DeleteCarViewModel {
    
    func deleteCarApiCall(userId: String, typeCode: String, completion: @escaping(Bool, String) -> Void) {
        
        AppInstance.showLoader()
        let base = Slimparts.baseURL
        let subBase = Slimparts.deleteCarFromListURL
        let code = "\(typeCode)/\(userId)"
        let url = "\(base)\(subBase)\(code)"
        
        AF.request(url, method: .get).responseJSON { (response) in
            
            switch response.result {
            case .success:
                guard let responseObj = response.value as? [String: Any], let status = responseObj["ResponseCode"] as? String, let message = responseObj["msg"] as? String else {
                    AppInstance.hideLoader()
                    completion(false, "Server problem! pleae try after some time")
                    return
                }
                if status == "1" {
                    AppInstance.hideLoader()
                    completion(true, message)
                } else {
                    AppInstance.hideLoader()
                    completion(false, message)
                }
            case .failure(let error):
                AppInstance.hideLoader()
                completion(false, "Server problem! please try after some time")
            }
            
        }
        
        
        
        
    }
    
}
