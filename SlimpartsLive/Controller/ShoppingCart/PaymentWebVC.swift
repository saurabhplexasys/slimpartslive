//
//  PaymentWebVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 23/02/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit
import PDFKit
import WebKit
import SwiftKeychainWrapper

class PaymentWebVC: UIViewController {

    var payData: PaymentModel?
    var cartList: ShoppingCartModel?
    var userDetails: UserDetailsModel?
    
    var apiHitPO = PlaceOrderViewModel()
    
    var userId = ""
    
    var webview = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.navigationItem.title = "Payment"
        if let userid = KeychainWrapper.standard.string(forKey: "userID") {
            userId = userid
        }
        webview = WKWebView(frame: UIScreen.main.bounds)
        view.addSubview(webview)
        webview.navigationDelegate = self
        if let webStr = payData?.payCheckOutUrl {
            webview.load(URLRequest(url: URL(string: webStr)!))
        } else {
            self.moveBack()
        }
        
        // Do any additional setup after loading the view.
    }
    
    func moveBack() {
        let alert = UIAlertController(title: "Server Problem!", message: "try after some time", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

}

extension PaymentWebVC: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
        AppInstance.showLoader()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        AppInstance.hideLoader()
        let url = webView.url?.absoluteString
        if url == "https://slimparts.nl/v1/placeorder" {
            webView.isHidden = true
            DispatchQueue.main.async {
                self.placeOrderApiCall(url: url!)
            }
        }
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func placeOrderApiCall(url: String) {
        apiHitPO.placeFinalOrderApiCall(userId: userId, url: url, payData: payData!, cartList: cartList!, userDetails: userDetails!) { (_isTrue, _message) in
            if _isTrue {
                self.showAlertAction(title: "Success", message: _message, alertStyle: .alert, actionTitles: ["Ok"], actionStyles: [.default], actions: [{_ in
                    self.moveToOrderScreen()
                    }])
            } else {
                self.showAlertAction(title: "Alert", message: _message, alertStyle: .alert, actionTitles: ["Ok"], actionStyles: [.default], actions: [{_ in
                    self.navigationController?.popViewController(animated: true)
                }])
            }
        }
    }
    
    func moveToOrderScreen() {
        KeychainWrapper.standard.removeObject(forKey: "notiCount")
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyOrdersVC") as! MyOrdersVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyOrdersVC") as! MyOrdersVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
