//
//  EngineTypeModel.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 31/12/20.
//  Copyright © 2020 SaurabhPlexaStudio. All rights reserved.
//

import Foundation

struct EngineTypeModel {
    var typeCode: String?
    var typeName: String?
    var kTypeCode: String?
}

