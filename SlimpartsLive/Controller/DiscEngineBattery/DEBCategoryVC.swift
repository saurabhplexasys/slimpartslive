//
//  DEBCategoryVC.swift
//  SlimpartsLive
//
//  Created by Dharmesh Kothari on 20/01/21.
//  Copyright © 2021 SaurabhPlexaStudio. All rights reserved.
//

import UIKit

class DEBCategoryVC: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var menuBtnOutlet: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var userHit = SaleViewModel()
    var catData = [SaleModel]()
    
    var searchData = [SaleModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = Slimparts.navBgColor
        self.setMenuBarBtn()
        tableView.delegate = self
        tableView.dataSource = self
        self.catAndSubCatApiCall()
        // Do any additional setup after loading the view.
    }
    
    func setMenuBarBtn() {
        menuBtnOutlet.target = self.revealViewController()
        menuBtnOutlet.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func catAndSubCatApiCall() {
        userHit.getAccessoriesListApi { (_isTrue, _message, _data) in
            if _isTrue {
                self.catData = _data
                self.searchData = self.catData
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } else {
                self.showAlert(title: "Alert", message: _message)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            searchData = catData
            self.tableView.reloadData()
            return
        }
        searchData = catData.filter({ (CatTitle) -> Bool in
            (CatTitle.catName?.lowercased().contains(searchText.lowercased()))!
        })
        self.tableView.reloadData()
    }
    
    
    
}


extension DEBCategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searchData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SaleTVCell", for: indexPath) as! SaleTVCell
        
        cell.titleLbl.text = searchData[indexPath.row].catName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if #available(iOS 13.0, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SaleSubVC") as! SaleSubVC
            vc.catData = searchData[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SaleSubVC") as! SaleSubVC
            vc.catData = searchData[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
